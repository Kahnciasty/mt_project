## Running from source code
The application was developed on python3.7
The project may be downloaded from Gitlab repository [here](https://gitlab.com/Kahnciasty/mt_project).

1. Install packages from `requirements.txt`. 
2. In case of installation errors regarding package PyQt4, follow instructions [here](https://stackoverflow.com/a/22651895).
3. Run `main.py`

## Running an executable on Windows
After unzipping the .zip file run `.\app\main.exe`. 

## Using the app
In order to load a match into the application, a user needs to select 1 out of 4 available files:
1. `GAME_ID.dat` file that contains tracking data
2. `GAME_ID_metadata.xml` file that contains metadata about the tracking data
3. `...-GAME_ID-eventdetails.xml` file that contains list of events from the match
4. `...-GAME_ID-matchresults.xml` file that contains general information about the match.


All of those files **need** to be in the same directory. The files with results of the algorithm (improved events, aligned tracking data, and a summary will be also saved into this directory)


After the game selection, the user has to load the game and run the animation. Keep in mind that the fact that the game has been loaded does not necessarily mean that it has been synchronized. The user may have to run the synchronization algorithm from the `sync` tab in the main window of the application.



## Synchronization without the app
To synchronize data without the usage of the application, simply run script `syncWithoutGui.py`. This script
takes two arguments:
 - The directory with the game files,
 - game ID.

Example of usage: `python syncWithoutGUI.py D:\path\to\directory\with\files 1059337`
Files with results are saved in the save directory as the original files.