import MathHelper
from typedefs.AttackDirection import AttackDirection
from objects.Coord import Coord
from typedefs.TeamType import TeamType
import numpy as np


class TrackingPlayer:
	def __init__(self, team, systemId, number, xCoord, yCoord, speed):
		self.number = number
		self.coord = Coord(xCoord, yCoord)
		self.systemId = systemId
		self.team_type = team
		self.speed = speed
		self.norm_speed_vector = []
		self.speed_vector = []  # [m/s]
		self.reaction_time = 0.7  # [s]
		self.v_max = 5  # [m/s]
		self.PPCF = 0.  # players contribution to total pitch control of a team
		self.time_to_intercept = 0
		self.lambda_ball_control = 4.3  # ball control parameter for pitch control
		self.tti_sigma = 0.45  # Standard deviation of sigmoid function in Spearman 2018 ('s') that determines uncertainty in player arrival time

		self.time_to_control = 3 * np.log(10) * (np.sqrt(3) * self.tti_sigma / np.pi + 1 / self.lambda_ball_control)

	def is_referee(self):
		return self.team_type == 4 or self.team_type == -1

	def is_player(self):
		return not self.is_referee()

	def is_offside(self, game, frame):
		if self.team_type == TeamType.AWAY:
			opposite_teamtype = TeamType.HOME
		else:
			opposite_teamtype = TeamType.AWAY

		last_defender_tracking = frame.get_last_defender(game, opposite_teamtype)
		this_event_player = game.get_event_player_by_tracking_player(self)
		if (game.get_period_id_of_frame(frame) == 1 and this_event_player.team.first_half_direction == AttackDirection.LEFT) or (
				game.get_period_id_of_frame(frame) == 2 and this_event_player.team.second_half_direction == AttackDirection.LEFT):
			return last_defender_tracking.coord.x > self.coord.x
		else:
			return last_defender_tracking.coord.x < self.coord.x

	# def get_time_to_control(self,game,frame):
	# 	if self.is_offside(game,frame):
	# 		self.lambda_ball_control = 0
	#
	# 	return 3 * np.log(10) * (np.sqrt(3) * self.tti_sigma / np.pi + 1 / self.lambda_ball_control)

	def initialize_speed_vectors(self, frame, game):
		if len(self.speed_vector) == 0 or len(self.speed_vector) == 0:
			self.norm_speed_vector = self.get_speed_vector_from_frame(frame, game)
			self.speed_vector = self.speed * self.norm_speed_vector  # [m/s]
			if self.is_offside(game,frame):
				self.lambda_ball_control = 0 # when player offside, the probability that he will control the ball is 0, so that he doesn't contribute to PPCF

	def __str__(self):
		return 'Number: {0}, x: {1}, y: {2}'.format(self.number, self.coord.x, self.coord.y)

	def is_between_own_goal_and_ball(self, opposite_team_attack_direction, ball):
		if opposite_team_attack_direction == AttackDirection.LEFT:
			return self.coord.x < ball.coord.x
		else:
			return self.coord.x > ball.coord.x

	def compute_simple_time_to_intercept(self, intercept_point, frame, game):
		self.PPCF = 0.
		# Time to intercept assumes that the player continues moving at current velocity for 'reaction_time' seconds
		# and then runs at full speed to the target position.

		self.initialize_speed_vectors(frame, game)
		end_reaction_coord = Coord(self.coord.x + self.speed_vector[0] * 100 * self.reaction_time,
								   self.coord.y + self.speed_vector[1] * 100 * self.reaction_time)  # *100 because end reaction coord is in cm and speed vector in m/s
		time_to_intercept = self.reaction_time + MathHelper.dist_between(intercept_point, end_reaction_coord) / (self.v_max * 100)  # *100 cause coord is in cm, but speed in m/s

		self.time_to_intercept = time_to_intercept
		self.end_reaction_coord = end_reaction_coord
		return self.time_to_intercept

	def get_speed_vector_from_frame(self, frame, game):
		frame_idx = game.get_index_by_id(frame.id)

		dx_s = []
		dy_s = []
		for j in range(0, 5):

			try:
				player = game.get_tracking_player_by_frame_index_and_track_player(frame_idx - j, self)
				prev_player = game.get_tracking_player_by_frame_index_and_track_player(frame_idx - j - 1, self)
			except IndexError:
				return [0, 0]

			if player is None or prev_player is None:
				continue

			dx_s.append(player.coord.x - prev_player.coord.x)
			dy_s.append(player.coord.y - prev_player.coord.y)

		array = [np.average(dx_s), np.average(dy_s)]
		norm = np.linalg.norm(array)
		return array / norm

	def probability_intercept_ball(self, T):
		# probability of a player arriving at target location at time 'T' given their expected time_to_intercept (time of arrival)
		f = 1 / (1. + np.exp(-np.pi / np.sqrt(3.0) / self.tti_sigma * (T - self.time_to_intercept)))
		return f


def from_line(participant_as_line) -> "TrackingPlayer":
	items = participant_as_line.split(",")
	if int(items[0]) == 1:
		team = TeamType.HOME
	elif int(items[0]) == 0:
		team = TeamType.AWAY
	else:
		team = TeamType.UNDEFINED
	systemId = int(items[1])
	number = int(items[2])
	xCoord = int(items[3])
	yCoord = int(items[4])
	speed = float(items[5])
	return TrackingPlayer(team, systemId, number, xCoord, yCoord, speed)
