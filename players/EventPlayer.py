class EventPlayer:
	def __init__(self, formation_place, player_ref, position, sub_position, number, status, captain, team_type, team):
		self.formation_place = int(formation_place)
		self.player_ref = player_ref
		self.position = position
		self.sub_position = sub_position
		self.number = int(number)
		self.status = status
		self.captain = captain is not None
		self.team_type = team_type
		self.team = team
		self.first_name = ""
		self.last_name = ""
		# if captain is None:
		# 	self.captain = False
		# else:
		# 	self.captain = True

	def is_captain(self):
		return self.captain

	def is_starting_11(self):
		return self.formation_place != 0

	def __str__(self):
		return "{0} {1} {2}".format(self.last_name, str(self.number), self.team.name)
