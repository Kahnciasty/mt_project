from sequence.SequenceAlignDirection import SequenceAlignDirection


class SequenceAligner:
	def __init__(self, match, mismatch, gap):
		self.match = match
		self.mismatch = mismatch
		self.gap = gap

	# Needleman–Wunsch algorithm
	def align(self, seq1, seq2):
		matrix = [[Cell() for _ in range(len(seq2) + 1)] for _ in range(len(seq1) + 1)]
		self.initialize_matrix(matrix)

		for j in range(1, len(matrix[0])):  # j - column
			for i in range(1, len(matrix)):  # i - row
				bool = seq1[i - 1] == seq2[j - 1]
				diag = self.match if bool else self.mismatch
				diag += matrix[i - 1][j - 1].value
				horiz = self.gap + matrix[i][j - 1].value
				vert = self.gap + matrix[i - 1][j].value
				matrix[i][j].value = max(diag, horiz, vert)

				if matrix[i][j].value == diag:
					matrix[i][j].directions.append(SequenceAlignDirection.DIAGONAL)

				if matrix[i][j].value == horiz:
					matrix[i][j].directions.append(SequenceAlignDirection.LEFT)

				if matrix[i][j].value == vert:
					matrix[i][j].directions.append(SequenceAlignDirection.TOP)

		solutions = self.traverse_matrix(matrix, seq1, seq2)
		return solutions

	def initialize_matrix(self, matrix):
		for i in range(1, len(matrix[0])):
			matrix[0][i].value = matrix[0][i - 1].value + self.gap
			matrix[0][i].directions.append(SequenceAlignDirection.LEFT)

		for i in range(1, len(matrix)):
			matrix[i][0].value = matrix[i - 1][0].value + self.gap
			matrix[i][0].directions.append(SequenceAlignDirection.TOP)

	def traverse_matrix(self, matrix, seq1, seq2):

		last_split_cell = None
		results = []

		while True:
			result1 = []
			result2 = []
			i = len(matrix) - 1  # row
			j = len(matrix[0]) - 1  # column
			while i != 0 or j != 0:
				cell = matrix[i][j]

				if cell.directions[0] == SequenceAlignDirection.DIAGONAL:
					i -= 1
					j -= 1
					result1.append(seq1[i])
					result2.append(seq2[j])

				elif cell.directions[0] == SequenceAlignDirection.LEFT:
					j -= 1
					result1.append(None)
					result2.append(seq2[j])

				elif cell.directions[0] == SequenceAlignDirection.TOP:
					i -= 1
					result1.append(seq1[i])
					result2.append(None)

				if len(cell.directions) > 1:
					last_split_cell = cell

			result1.reverse()
			result2.reverse()
			results.append(AlignSolution(result1, result2))

			# if lack of split cells - break and return the result
			if last_split_cell is None:
				break

			# remove used direction from the last split cell and traverse the matrix again
			last_split_cell.directions.pop(0)
			last_split_cell = None

		return results

	def print_summary(self, solutions):
		for s1, s2 in solutions:
			print(s1)
			print(s2)
			matches = 0
			gaps = 0
			mismatches = 0

			print("Matches - {0}".format(matches))
			print("Mismatches - {0}".format(mismatches))
			print("Gaps - {0}".format(gaps))
			print()


class Cell(object):
	def __init__(self):
		self.value = 0
		self.directions = []

	def __str__(self):
		dirs = ""
		for d in self.directions:
			dirs += d.name + "; "

		return "value: {0}; directions: {1}".format(self.value, dirs)


class AlignSolution(object):
	def __init__(self, r1, r2):
		self.pass_performers = r1
		self.possessors = r2
