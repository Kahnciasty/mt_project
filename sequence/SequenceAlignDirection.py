from enum import Enum


class SequenceAlignDirection(Enum):
	TOP = 1,
	LEFT = 2,
	DIAGONAL = 3