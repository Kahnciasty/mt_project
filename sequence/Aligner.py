import datetime
import random
import time

import MathHelper
from typedefs.AttackDirection import AttackDirection
from objects.Event import Event
from typedefs.EventType import EventType
from typedefs.QuallifierType import QualifierType
from sequence.SequenceAligner import SequenceAligner
from objects.Statistics import Statistics
import numpy as np
import itertools

from typedefs.TeamType import TeamType


class Aligner:
	def __init__(self, game):
		self.state_changing_events = [EventType.OUT,
									  EventType.CORNER_AWARDED,
									  EventType.FOUL,
									  EventType.CAUGHT_OFFSIDE,
									  EventType.GOAL,
									  EventType.START,
									  EventType.END]
		self.pass_events = [EventType.PASS,
							EventType.OFFSIDE_PASS,
							EventType.CLEARANCE,
							EventType.MISS,
							EventType.POST,
							EventType.ATTEMPT_SAVED,
							EventType.BALL_TOUCH]
		self.duel_events = [EventType.AERIAL,
							EventType.TACKLE,
							EventType.TAKE_ON,
							EventType.CHALLENGE,
							EventType.DISPOSSESSED]
		self.recovery_events = [EventType.BALL_RECOVERY,
								EventType.INTERCEPTION,
								EventType.KEEPER_PICK_UP,
								EventType.KEEPER_SWEEPER,
								EventType.CROSS_NOT_CLAIMED,
								EventType.SMOTHER]
		self.reception_events = [EventType.PASS,
								 EventType.OFFSIDE_PASS,
								 EventType.MISS,
								 EventType.POST,
								 EventType.GOAL,
								 EventType.ATTEMPT_SAVED,
								 EventType.BALL_RECOVERY,
								 EventType.INTERCEPTION,
								 EventType.FOUL]
		# punch? keeper_pickup?
		self.game = game
		self.possession_threshold = 150  # [cm] sphere of influence radius
		self.max_ball_height = 250
		self.stats = Statistics()
		self.max_min = 100  # max minute of an analysed event when adjusting
		self.event_separator = ";"
		self.the_same_solutions = 0
		self.total_solutions = 0

	def generate_file_without_synchronization(self):
		# print("Async started")
		with open(self.game.location_data_path, 'r') as file:
			data = file.readlines()

		for event in self.game.events:
			idx = self.get_event_frame_index(event)
			try:
				self.add_event_to_frame(data, event, idx)
			except IndexError:
				continue

		with open(self.game.asynced_location_data_path, 'w') as file:
			file.writelines(data)
		# print("Async concluded")

	def adjust_events(self):
		self.clear_stats()
		for event in self.game.events:
			event.frame_id = None

		start = time.time()
		with open(self.game.location_data_path, 'r') as file:
			# data contains all lines from the tracking .dat files. The IDs of aligned events will be added at the end of a frame
			data = file.readlines()

		self.check_correct_sides()

		self.adjust_state_events_and_passes(data)
		self.adjust_aerial_events(data)
		self.adjust_recovery_events(data)

		self.create_reception_and_carry_events(data)

		self.game.events.sort(key=lambda event: (event.period_id, event.min, event.sec, event.same_minute_order()))
		# self.game.events.sort(key=lambda event: (event.frame_id))
		# self.game.events.sort(
		# 	key=lambda event: (event.display_order(self.game.position_frequency, self.game.first_period_frame_id)))

		self.improve_chosen_events(data)

		self.count_event_stats()

		self.game.aligning_stats = self.stats

		with open(self.game.synced_location_data_path, 'w') as file:
			file.writelines(data)

		elapsed_time = time.time() - start
		# print("Adjusting events took {0:.2f} seconds".format(elapsed_time))
		# print("same solutions {0}".format(self.the_same_solutions))
		# print("total solutions {0}".format(self.total_solutions))
		# print("State Changing Events: {0}".format(len(self.stats.state_changing_events)))
		# print("Pass Events: {0}".format(len(self.stats.pass_events)))
		# print("Duel Events: {0}".format(len(self.stats.duel_events)))
		# print("Recovery Events: {0}".format(len(self.stats.recovery_events)))

	def adjust_state_events_and_passes(self, data):
		counter = 0
		pass_performers = []
		pass_events = []
		last_state_event = self.get_start_event(self.game.events)
		for event in self.game.events:
			# add pass events to a list. These events will be later on adjusted between surrounding state changing events
			if event.type in self.pass_events:
				pass_performers.append(event.player)
				pass_events.append(event)
				self.stats.pass_events.append(event)

			if event.type in self.state_changing_events:
				self.stats.state_changing_events.append(event)
				state_changing_margin = self.game.position_frequency * 2
				self.adjust_state_changing_events(event, state_changing_margin, data)

				# don't analyze time before start and during game break
				if event.type == EventType.START:
					pass_performers = []
					pass_events = []
					last_state_event = event
					continue

				last_state_event_idx = self.get_event_frame_index(last_state_event)
				event_idx = self.get_event_frame_index(event)
				if event_idx == last_state_event_idx:
					continue
				counter += 1

				poss_margin = self.game.position_frequency * 2
				# From tracking data detect possession order and index of a frame when the possession ends
				# events matched to a tracking possession will be assigned to the frame corresponding to the en of possession
				poss_seq, idx_seq = self.get_possession_sequence(last_state_event_idx - poss_margin,
																 event_idx + poss_margin)

				aligner = SequenceAligner(match=1, mismatch=-2, gap=-1)
				# align pass performers to ball possessors
				solutions = aligner.align(pass_performers, poss_seq)
				solution = self.select_best_solution(idx_seq, pass_events, solutions)
				self.adjust_events_from_solution(data, idx_seq, pass_events, solution)

				last_state_event = event
				pass_performers = []
				pass_events = []
				if last_state_event.min > self.max_min:
					break

	# idx_seq - sequence of indices of frames that end in possession from tracking data solution.pass_performers -
	# sequence of pass performers from event data aligned to the idx_seq.
	# At this stage those two sequences should be
	# aligned with each other. So both of them may contain None values, when the alignment found a gap.
	def adjust_events_from_solution(self, data, idx_seq, pass_events, solution):
		q = 0
		for i in range(len(solution.pass_performers)):
			if solution.pass_performers[i] is not None:
				try:
					idx_seq[i - q]
					solution.possessors[i]
				except IndexError:
					# print("idx_error")
					continue

				# When a possessor is none in a solution, the idx_seq does not reflect the possession order anymore. This is why we have to compensate it with 'q' factor
				if solution.possessors[i] is None:
					q += 1
					self.add_mismatch(data, pass_events.pop(0), idx_seq[i - q])
					continue
				# if pass performer does not match aligned possessors report mismatch
				if solution.pass_performers[i] is not solution.possessors[i]:
					event = pass_events.pop(0)
					self.change_event_player(event, solution.possessors[i])

					frame = self.game.frames[idx_seq[i - q]]
					track_player = self.game.get_tracking_player_from_event(event, frame)
					if track_player is not None:
						self.game.set_event_position_by_track_player(event, track_player)

					self.add_mismatch(data, event, idx_seq[i - q])
					continue
				# correct alignment. When an the alignment matches, just pop event from the event list and assign it to a frame from idx_seq.
				if idx_seq[i - q] is not None:
					event = pass_events.pop(0)
					self.add_pass_event_to_frame(data, event, idx_seq[i - q])

					frame = self.game.frames[idx_seq[i - q]]
					track_player = self.game.get_tracking_player_from_event(event, frame)
					if track_player is not None:
						self.game.set_event_position_by_track_player(event, track_player)

	def change_event_player(self, event, track_player):
		event_player = self.game.get_event_player_by_tracking_player(track_player)
		event.player = event_player
		event.player_id = event_player.player_ref
		event.player_corrected = True
		event.team = event_player.team
		event.team_id = event_player.team.id
		self.stats.corrected_performers.append(event)


	# Needleman Wunsch gives all best alignments from the point of view sequence matching. But those alignments are not
	# exactly equal from the point of view of the datasets. We have to decide which of those alignments are the best
	# considering the situation on the pitch. Currently, the solution that will be selected as the best will be the
	# one that has the lowest average distance between the aligned players (in the moment of aligned event) and the
	# original event location (taken directly from the event data)
	def select_best_solution(self, idx_seq, events, solutions):
		avg_dist = []
		avg_frames_diff = []
		for aligned_seqs in solutions:
			seq_dist = []
			seq_frames = []
			temp_pass_events = list(events)
			for i in range(len(aligned_seqs.pass_performers)):
				# there is more possessors than pass performers, so after alignment there will be some none
				# values in pass performers list. It is because in fact performers are moved towards
				# possessors during alignment algorithm
				if aligned_seqs.pass_performers[i] is not None:
					try:
						z = idx_seq[i]
					except IndexError:
						# print("index-error")
						continue

					if idx_seq[i] is not None:
						temp_event = temp_pass_events.pop(0)
						d = self.get_player_event_distance(idx_seq[i], temp_event)
						seq_dist.append(d)

						f = self.get_frame_difference(idx_seq[i], temp_event)
						seq_frames.append(f)
			avg_dist.append(np.average(seq_dist))
			avg_frames_diff.append(np.average(seq_frames))
		# frame index difference gives for the most part very similar as distance from event to event performer

		if np.argmin(avg_dist) == np.argmin(avg_frames_diff):
			self.the_same_solutions += 1

		self.total_solutions += 1
		best_solution_idx = np.argmin(avg_dist)
		# best_solution_idx = np.argmin(avg_frames_diff)
		return solutions[best_solution_idx]

	def adjust_aerial_events(self, data):
		for i in range(len(self.game.events)):
			event = self.game.events[i]
			if event.type in self.duel_events and not event.is_aligned():
				self.stats.duel_events.append(event)
				aerials = [event, self.game.events[i + 1]]
				before_event, after_event = self.game.get_surrounding_aligned_events(event)
				idx = self.compute_idx_for_aerials(aerials, before_event, after_event)
				if idx is not None:
					self.add_event_to_frame(data, aerials[0], idx)
					self.add_event_to_frame(data, aerials[-1], idx)
					track_player = self.game.get_tracking_player_from_event(aerials[0], self.game.frames[idx])
					if track_player is not None:
						self.game.set_event_position_by_track_player(aerials[0], track_player)
					track_player2 = self.game.get_tracking_player_from_event(aerials[-1], self.game.frames[idx])
					if track_player2 is not None:
						self.game.set_event_position_by_track_player(aerials[-1], track_player2)

			if event.min > self.max_min:
				break

	def compute_idx_for_aerials(self, aerials, before_event, after_event):
		distances = []
		start = self.game.get_index_by_id(before_event.frame_id) - self.game.position_frequency * 3
		stop = self.game.get_index_by_id(after_event.frame_id) + self.game.position_frequency * 3
		for i in range(start, stop):
			track_player1 = self.game.get_tracking_player_from_event(aerials[0], self.game.frames[i])
			track_player2 = self.game.get_tracking_player_from_event(aerials[-1], self.game.frames[i])
			ball = self.game.frames[i].ball
			# sometimes a player temporarily disappears from tracking data. Then track_player_X is None
			dists = []
			try:
				dists = [MathHelper.dist_between(track_player1.coord, track_player2.coord),
						 MathHelper.dist_between(ball.coord, track_player2.coord),
						 MathHelper.dist_between(track_player1.coord, ball.coord)]
			except AttributeError:
				distances.append(np.Infinity)
				continue
			distances.append(sum(dists))

		if not distances:
			return None
		index = start + np.argmin(distances)
		return index

	def get_player_event_distance(self, idx, temp_event):
		track_player = self.game.get_tracking_player_from_event(temp_event,
																self.game.frames[idx])

		# if player from event is not recorded in tracking data frame, return dist Inf
		try:
			x = track_player.coord
			x = temp_event.coord
		except AttributeError:
			return float('Inf')

		return MathHelper.dist_between(track_player.coord, temp_event.coord)

	def get_frame_difference(self, idx, temp_event):
		period = temp_event.period_id if 1 <= temp_event.period_id <= 2 else 0
		orig_idx = self.game.get_index_by_time(temp_event.min, temp_event.sec, period)
		return abs(orig_idx - idx)

	def get_event_frame_index(self, event):
		event_time_in_sec_since_period_start = MathHelper.calculate_event_time_since_period_start(event)
		frames_since_period_start_to_event = event_time_in_sec_since_period_start * self.game.position_frequency

		event_period = event.period_id
		event_period = min(event_period, 2)

		if event_period == 1:
			event_frame_index = self.game.get_index_by_id(
				self.game.first_period_frame_id) + frames_since_period_start_to_event
		else:
			event_frame_index = self.game.get_index_by_id(
				self.game.second_period_frame_id) + frames_since_period_start_to_event
		return event_frame_index

	# the state changing events are assigned to a frame when the ball changes state of the game from alive to dead
	def adjust_state_changing_events(self, event, margin, data):
		if event.period_id != 1 and event.period_id != 2:
			return

		event_frame_index = self.get_event_frame_index(event)
		new_index = event_frame_index  # in case that there is no status change in the range we were looking in
		for i in range(event_frame_index - margin,
					   event_frame_index + margin):
			if i > len(self.game.frames):
				new_index = event_frame_index
				break

			try:
				if self.game.frames[i].ball.is_dying(self.game.frames[i + 1]):
					new_index = i
					break
			except IndexError:
				continue
				# print("error123")

		self.add_event_to_frame(data, event, new_index)
		self.stats.state_alignment_stats.append(event_frame_index - new_index)
		self.update_event_diff_dict(event_frame_index - new_index)

	def add_event_to_frame(self, data, event, index, mismatched="F"):
		event.frame_id = self.game.frames[index].id
		self.improve_event_min_sec(event, index)
		data[index] = "{0}{1},{2}{3}{4}".format(data[index].rstrip(), event.id, mismatched, self.event_separator, "\n")

	def improve_event_min_sec(self, event, idx):
		frame = self.game.frames[idx]
		sec = self.get_second_from_frame(frame)
		minutes = int(sec / 60)
		seconds = int(sec - minutes * 60)
		event.min = minutes
		event.sec = seconds

	def get_stats(self):
		return self.pass_std_dev

	def clear_stats(self):
		self.stats = Statistics()

	def update_event_diff_dict(self, diff):
		try:
			self.stats.event_diff_dict[diff] += 1
		except KeyError:
			self.stats.event_diff_dict[diff] = 1


	# possession sequence from the tracking data is given based on a possession_threshold. Every player that is closer
	# to the ball than the possession threshold is added to the possession sequence
	def get_possession_sequence(self, start, stop):
		sequence = []
		poss_end_indices = []
		for i in range(max(start, 0), stop):
			track_player = self.game.get_tracking_player_closest_to_ball(i)
			ball = self.game.frames[i].ball

			try:
				dist = MathHelper.dist_between(track_player.coord, ball.coord)
			except AttributeError:
				continue
				# print("attr error")

			if dist < self.possession_threshold and ball.coord.z < self.max_ball_height:
				event_player = self.game.get_event_player_by_tracking_player(track_player)
				if not sequence or sequence[-1] != event_player:
					sequence.append(event_player)
					idx = self.get_possession_end_index(track_player, i)
					poss_end_indices.append(idx)
					continue
		return sequence, poss_end_indices

	def get_start_event(self, events):
		for event in events:
			if event.type == EventType.START:
				return event

	# possession end index is selected as the last frame when the ball is closer to the possessor than the
	# possession_threshold, but only if the next possessor is different than the current one. It may happen that the
	# ball leaves the sphere of influence of the player, but a few frames later the ball comes back within the
	# possession_threshold. (eg. a player is not contested, he received a pass, but upon reception the ball bounced of
	# his leg for 2 meters. Since the player is not contested, he will easily regain control of the ball by quickly
	# running towards it). Such incident will look in the possession sequence as two separate possession in a row of
	# the same player. The possession_end_index will not be fooled by such events and only the last cross of the
	# possession_threshold will be recorded as end of possession
	def get_possession_end_index(self, track_player, idx):
		loaded_lines = self.game.number_of_lines_to_load - 1
		upper_bound = min(idx + self.game.position_frequency * 20, loaded_lines)
		for i in range(idx, upper_bound):
			track_player = self.game.get_tracking_player_by_frame_index_and_track_player(i, track_player)
			next_player = self.game.get_tracking_player_closest_to_ball(i + 1)

			d = MathHelper.dist_between(next_player.coord, self.game.frames[i + 1].ball.coord)
			if next_player is not None and track_player is not None and next_player.systemId != track_player.systemId and d < self.possession_threshold:
				for j in range(i, i - self.game.position_frequency * 10, -1):
					ball = self.game.frames[j].ball
					track_player = self.game.get_tracking_player_by_frame_index_and_track_player(j, track_player)
					dist = MathHelper.dist_between(track_player.coord, ball.coord)
					if dist < self.possession_threshold and ball.coord.z < self.max_ball_height:
						return j
		# If no possession end index was found, then return the latest possible index
		return upper_bound

	def add_pass_event_to_frame(self, data, event, idx):
		self.add_event_to_frame(data, event, idx)

		orig_index = self.get_event_frame_index(event)
		self.stats.pass_alignment_stats.append(orig_index - idx)
		self.update_event_diff_dict(orig_index - idx)

	def add_mismatch(self, data, event, idx):
		self.add_event_to_frame(data, event, idx, "T")
		self.stats.mismatched_events.append(event)
		event.mismatched = True

	def count_event_stats(self):
		adjusted = 0
		mismatched = 0
		for event in self.game.events:
			if event.frame_id is not None:
				adjusted += 1
			if event.mismatched:
				mismatched += 1

		# print("Mismatched {0} events. Which is {1:.2f}% of total".format(mismatched,
		# 																 mismatched / len(self.game.events) * 100))
		# print("Adjusted {0} events. Which is {1:.2f}% of all events".format(adjusted,
		# 																	adjusted / len(self.game.events) * 100))
		# print("Corrected performers: {0}".format(self.stats.corrected_performers))

	def adjust_recovery_events(self, data):
		for i in range(len(self.game.events)):
			event = self.game.events[i]
			if event.type in self.recovery_events and not event.is_aligned():
				self.stats.recovery_events.append(event)

				before_event, after_event = self.game.get_surrounding_aligned_events(event)
				idx = self.compute_idx_for_recovery(event, before_event, after_event)
				if idx is not None:
					self.add_event_to_frame(data, event, idx)
					track_player = self.game.get_tracking_player_from_event(event, self.game.frames[idx])
					if track_player is not None:
						self.game.set_event_position_by_track_player(event, track_player)

			if event.min > self.max_min:
				break

	# recovery frame is the same frame when the ball approaches a player for a distance shorter than the
	# possession_threshold
	def compute_idx_for_recovery(self, event, before_event, after_event):
		start = self.game.get_index_by_id(before_event.frame_id) - self.game.position_frequency * 3
		stop = self.game.get_index_by_id(after_event.frame_id) + self.game.position_frequency * 3
		distances = []
		for idx in range(start, stop):
			frame = self.game.frames[idx]
			ball = frame.ball
			player = self.game.get_tracking_player_from_event(event, frame)
			dist = MathHelper.dist_between(ball.coord, player.coord)
			distances.append(dist)
			if dist < self.possession_threshold:
				return idx

	# every possession of the ball should end with an event from self.reception_events or with a foul. But if the
	# possession ends, we know that it also had to start. So we take every reception_event and we go backwords frame
	# by frame and we try to detect the reception event (defined as first time(after a previous event) when the
	# distance between the event performer and the ball crossed the possession_threshold)
	def create_reception_and_carry_events(self, data):
		aligned_events = self.game.get_aligned_events()
		for idx, event in enumerate(aligned_events):
			# a foul may end a possession, but it comes in pairs - one foul event for every team. we want to treat the
			# foul there as if it happened only one in one moment of the game
			if event.type not in self.reception_events or aligned_events[idx + 1].type == EventType.FOUL:
				continue
			prev_event = aligned_events[idx - 1]
			prev_frame_idx = self.game.get_index_by_id(prev_event.frame_id)

			for i in range(0, self.game.position_frequency * 10):
				frame = self.game.frames[prev_frame_idx + i]
				track_player = self.game.get_tracking_player_from_event(event, frame)

				if track_player is None:
					dist = np.Infinity
				else:
					dist = MathHelper.dist_between(track_player.coord, frame.ball.coord)

				if dist < self.possession_threshold:
					# Create new reception event
					reception = self.create_new_event(EventType.RECEPTION, frame, track_player)

					# there are already reception or carry events of given player in the same time, then don't add duplicates
					same_time_events = [eve for eve in self.game.events if
										eve.min == reception.min and eve.sec == reception.sec and eve.player == reception.player and (
												eve.type == EventType.RECEPTION or eve.type == EventType.CARRY)]
					if same_time_events:
						break

					self.assign_zone_to_event(reception)
					self.add_event_to_frame(data, reception, prev_frame_idx + i)
					self.add_closest_opponent_qualifier(reception, frame, track_player)
					event.reception_id = reception.id

					# create new carry event using reception and original event from event dataset
					carry = self.create_carry_event(event, frame, track_player)
					self.add_event_to_frame(data, carry, prev_frame_idx + i)

					self.game.events.append(carry)
					self.game.events.append(reception)
					break

	def create_new_event(self, event_type, frame, track_player):
		event_player = self.game.get_event_player_by_tracking_player(track_player)
		new_id = self.create_random_id()
		event_id = self.create_random_id()
		period_id = self.game.get_period_id_of_frame(frame)
		team_id = self.game.get_team_by_team_type(event_player.team_type).id
		event = Event(id=new_id, event_id=event_id, period_id=period_id,
					  player_id=event_player.player_ref, team_id=team_id)
		event.type = event_type
		event.frame_id = int(frame.id)
		sec = self.get_second_from_frame(frame)
		minutes = int(sec / 60)
		seconds = int(sec - minutes * 60)
		event.min = minutes
		event.sec = seconds
		event.coord = track_player.coord
		event.last_modified = datetime.datetime.now().isoformat()
		self.game.set_event_position_by_track_player(event, track_player)
		return event

	def create_carry_event(self, event, frame, track_player):
		carry = self.create_new_event(EventType.CARRY, frame, track_player)
		carry.add_qualifier(QualifierType.PASS_END_Y, event.y)
		carry.add_qualifier(QualifierType.PASS_END_X, event.x)
		return carry

	def get_second_from_frame(self, frame):
		if frame.id < self.game.second_period_frame_id:
			frames_num = frame.id - self.game.first_period_frame_id
		else:
			frames_num = frame.id - self.game.second_period_frame_id + 45 * 60 * self.game.position_frequency

		return int(frames_num / self.game.position_frequency)

	def add_closest_opponent_qualifier(self, event, frame, track_player):
		opponent_players = [player for player in frame.players if player.team_type != track_player.team_type]
		closest_opponent_dist = np.Infinity
		for player in opponent_players:
			dist = MathHelper.dist_between(track_player.coord, player.coord)
			closest_opponent_dist = min(closest_opponent_dist, dist)
		closest_opponent_dist /= 100  # [m]
		event.add_qualifier(QualifierType.CLOSEST_OPPONENT_DISTANCE, str(closest_opponent_dist))

	def create_random_id(self):
		return str(random.randint(0, 10000000))

	def assign_zone_to_event(self, event):
		if event.on_back_side():
			event.add_qualifier(QualifierType.ZONE, "Back")
		elif event.on_left_side():
			event.add_qualifier(QualifierType.ZONE, "Left")
		elif event.on_right_side():
			event.add_qualifier(QualifierType.ZONE, "Right")
		else:
			event.add_qualifier(QualifierType.ZONE, "Center")

		temp_x = event.x if event.on_front_side() else 100 - event.x
		if temp_x > 85:  # next to penalty box
			if event.on_left_side():
				event.add_qualifier(QualifierType.OUT_OF_BOX_DEEP_LEFT)
			elif event.on_right_side():
				event.add_qualifier(QualifierType.OUT_OF_BOX_DEEP_RIGHT)

			else:  # in penalty box - 5.5[m]/105[m]*100% - dist from end line to goalie box line = 5.24; 100-5.24=94.76
				if temp_x > 94.76:  # closer to a goal
					if event.y > 50 + 13.5:
						event.add_qualifier(QualifierType.SMALL_BOX_LEFT)
					elif event.y < 50 - 13.5:
						event.add_qualifier(QualifierType.SMALL_BOX_RIGHT)
					else:
						event.add_qualifier(QualifierType.SMALL_BOX_CENTRE)
				else:
					if event.y > 50 + 13.5:
						event.add_qualifier(QualifierType.BOX_LEFT)
					elif event.y < 50 - 13.5:
						event.add_qualifier(QualifierType.BOX_RIGHT)
					else:
						event.add_qualifier(QualifierType.BOX_CENTRE)

		elif temp_x < 65:  # closer to half line
			if event.on_left_side():
				event.add_qualifier(QualifierType.LEFT_35)
			elif event.on_right_side():
				event.add_qualifier(QualifierType.RIGHT_35)
			else:
				event.add_qualifier(QualifierType.CENTRE_35)

		else:  # x in the middle part of half
			if event.on_left_side():
				event.add_qualifier(QualifierType.OUT_OF_BOX_LEFT)
			elif event.on_right_side():
				event.add_qualifier(QualifierType.OUT_OF_BOX_RIGHT)
			else:
				event.add_qualifier(QualifierType.OUT_OF_BOX_CENTRE)

	def improve_chosen_events(self, data):
		events = [event for event in self.game.events if
				  event.type == EventType.PASS or event.type == EventType.CLEARANCE]
		self.add_height(events)

		pass_events = [event for event in self.game.events if event.type == EventType.PASS]
		self.add_packing_rate(pass_events)
		self.correct_end_coords(self.game.events)

		seq_events = [event for event in self.game.events if event.type in self.pass_events]
		self.add_passing_sequence(seq_events)

	def add_height(self, events):
		for event in events:
			previous_event, next_event = self.game.get_surrounding_aligned_events(event)
			if event is None or next_event is None or event.frame_id is None or next_event.frame_id is None:
				continue
			start_idx = self.game.get_index_by_id(event.frame_id)
			end_idx = self.game.get_index_by_id(next_event.frame_id)
			max_height = 0

			for idx in range(start_idx, end_idx):
				frame = self.game.frames[idx]
				ball_height = frame.ball.coord.z
				max_height = max(ball_height, max_height)
			event.add_qualifier(QualifierType.PASS_HEIGHT, max_height / 100)

	def add_packing_rate(self, events):
		for event in events:
			if not (event.is_successful() and event.is_aligned()):
				continue

			attacking_team_type = event.player.team_type
			reception = self.game.get_pass_reception(event)
			if reception is None:
				continue

			start_frame = self.game.get_frame_by_id(event.frame_id)
			end_frame = self.game.get_frame_by_id(reception.frame_id)

			opponents_list_before = [player for player in start_frame.players if
									 player.team_type != attacking_team_type and player.team_type != TeamType.UNDEFINED]
			attack_direction = event.determine_attack_direction()
			opponents_before = sum(1 for track_player in opponents_list_before if
								   track_player.is_between_own_goal_and_ball(attack_direction, start_frame.ball))

			opponents_list_after = [player for player in end_frame.players if
									player.team_type != attacking_team_type and player.team_type != TeamType.UNDEFINED]
			attack_direction = event.determine_attack_direction()
			opponents_after = sum(1 for track_player in opponents_list_after if
								  track_player.is_between_own_goal_and_ball(attack_direction, end_frame.ball))

			packing_rate = opponents_before - opponents_after
			event.add_qualifier(QualifierType.PACKING_RATE, packing_rate)  # [meters]

	# Sometimes the event coords are mirror imaged with center of the pitch as a point symmetry. We need find out
	# if this error takes place in the game. We do it by checking if the first "keeper pick up" event takes place
	# close to the goalie or not. If it is close the events have correct x and y coords. On the other hand,
	# if the event takes place on the other side of the pitch (further than half of the pitch width from the
	# goalie), we know that the event coords are the other way around than they should be. So we change the coords
	# of all events from event data
	def check_correct_sides(self):
		first_keeper_pickup = None
		for event in self.game.events:
			if event.type == EventType.KEEPER_PICK_UP:
				first_keeper_pickup = event
				break

		idx = self.game.get_index_by_time(first_keeper_pickup.min, first_keeper_pickup.sec,
										  first_keeper_pickup.period_id)
		frame = self.game.frames[idx]

		track_goalie = self.game.get_tracking_player_from_event(first_keeper_pickup, frame)

		self.correct_event_coords(track_goalie, first_keeper_pickup)
		self.assign_attack_direction(track_goalie)

	def correct_event_coords(self, track_goalie, first_keeper_pickup):
		dist = MathHelper.dist_between(track_goalie.coord, first_keeper_pickup.coord)
		width, _ = self.game.get_pitch_dimensions()
		width *= 100  # [m]

		if dist > width / 2:
			# Event coordinates are mirror imaged with center of the pitch as a point symmetry. We need to undo the mirror effect
			for event in self.game.events:
				event.x = 100 - event.x
				event.y = 100 - event.y

	def assign_attack_direction(self, track_goalie):
		event_goalie = self.game.get_event_player_by_tracking_player(track_goalie)
		goalie_team = event_goalie.team

		opposite_team = None
		for team in self.game.teams:
			if goalie_team != team:
				opposite_team = team
				break

		if track_goalie.coord.x < 0:
			goalie_team.first_half_direction = AttackDirection.RIGHT
			goalie_team.second_half_direction = AttackDirection.LEFT
			opposite_team.first_half_direction = AttackDirection.LEFT
			opposite_team.second_half_direction = AttackDirection.RIGHT
		else:
			goalie_team.first_half_direction = AttackDirection.LEFT
			goalie_team.second_half_direction = AttackDirection.RIGHT
			opposite_team.first_half_direction = AttackDirection.RIGHT
			opposite_team.second_half_direction = AttackDirection.LEFT

		start_events = [event for event in self.game.events if event.type == EventType.START]
		for event in start_events:
			q = event.get_qualifier(QualifierType.DIRECTION_OF_PLAY)
			if event.period_id == 1 and event.team_id == goalie_team.id:
				q.value = goalie_team.first_half_direction.value
			elif event.period_id == 1 and event.team_id == opposite_team.id:
				q.value = opposite_team.first_half_direction.value
			elif event.period_id == 2 and event.team_id == goalie_team.id:
				q.value = goalie_team.second_half_direction.value
			elif event.period_id == 2 and event.team_id == opposite_team.id:
				q.value = opposite_team.second_half_direction.value

	def add_passing_sequence(self, events):
		seq = 0
		for key, group in itertools.groupby(events, lambda event: event.team_id):
			for e in group:
				e.add_qualifier(QualifierType.PASS_SEQUENCE, seq)
			seq += 1

	def correct_end_coords(self, events):
		for idx, event in enumerate(events):
			if event.type != EventType.PASS or not event.is_successful():
				continue

			reception = self.get_next_reception(events, idx)
			if reception is not None:
				event.add_qualifier(QualifierType.PASS_END_X, reception.x)
				event.add_qualifier(QualifierType.PASS_END_Y, reception.y)

	def get_next_reception(self, events, idx):
		for i in range(idx, idx + 10):
			try:
				if events[i + 1].type == EventType.RECEPTION:
					return events[i + 1]
			except IndexError:
				return None

		return None
