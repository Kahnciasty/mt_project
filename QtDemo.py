import sys
from PyQt4.QtGui import QApplication, QMainWindow

from GUI.GUI.MainWindow import Ui_MainWindow


class MainWindow(QMainWindow):
	def __init__(self):
		super(MainWindow, self).__init__()
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.ui.pushButton.clicked.connect(self.clicked)
		# self.pushButton.clicked.connect(clicked)

	def clicked(self):
		self.ui.label.setText("siema")

if __name__ == '__main__':
	app = QApplication(sys.argv)

	window = MainWindow()
	window.show()
	sys.exit(app.exec_())