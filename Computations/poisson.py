import json
import random

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def poisson():
	# with open('D:\wyscout\wyscout-soccer-match-event-dataset\\raw_data\matches\matches_World_Cup.json') as f:
	with open('D:\wyscout\wyscout-soccer-match-event-dataset\\raw_data\matches\matches_Germany.json') as f:
		data = json.load(f)
	pd.set_option('display.max_columns', None)
	# Create a data set of shots.
	df = pd.DataFrame(data)

	teams_data_df = pd.DataFrame(df['teamsData'])

	goals_no = []

	for i, team in teams_data_df.iterrows():
		goals = 0
		for key in team['teamsData']:
			score  = team['teamsData'][key]['score']
			goals += score
		goals_no.append(goals)

	avg_goals = np.average(goals_no)
	print("Avg goals per game: ", avg_goals)

	prob_goal_minute = avg_goals / 90
	goals_no_simulation = simulation(prob_goal_minute, len(goals_no))

	# fig, ax = plt.subplots(1, 1, tight_layout=True)

	plt.hist(goals_no,bins=8, rwidth=0.7, align='left')
	# plt.hist(goals_no_simulation,bins=8, rwidth=0.7, align='mid')
	hist = np.histogram(goals_no_simulation, bins=np.max(goals_no_simulation))
	# hist[0] = np.append(hist[0], 0)
	plt.plot( hist[0])

	plt.title('histogram of # of goals in one game')
	plt.xlabel('This many goals')
	plt.ylabel('In this many games')
	plt.show()


	print('slut')


def simulation(prob, games_no):
	goals_no = []
	for i in range(games_no):
		goals_in_game = 0
		for minute in range(90):
			if random.random() < prob:
				goals_in_game += 1
		goals_no.append(goals_in_game)

	return goals_no


if __name__ == '__main__':
	poisson()