import json
import math

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from sklearn.linear_model import LogisticRegression


def passes():

	with open('D:\wyscout\wyscout-soccer-match-event-dataset\\raw_data\events\events_World_Cup.json') as f:
		# with open('D:\wyscout\wyscout-soccer-match-event-dataset\\raw_data\events\events_Germany.json') as f:
		data = json.load(f)
	pd.set_option('display.max_columns', None)
	# Create a data set of shots.
	df = pd.DataFrame(data)

	passes_df = df[df['eventName'] == 'Pass'].head(50000)
	successes = []
	distances = []
	angles = []
	for i, p in passes_df.iterrows():
		if i == 5000:
			print('Preprocessing: ', i)
		# Add Success column
		if len(p['tags']) == 0:
			successes.append(False)
		else:
			successes.append(p['tags'][0]['id'] == 1801)

		#Add distance columns
		if len(p['positions']) == 0:
			distances.append(0)
		else:
			y_length = p['positions'][1]['y'] - p['positions'][0]['y']
			x_length = p['positions'][1]['x'] - p['positions'][0]['x']
			distances.append(math.sqrt((y_length)**2 + (x_length)**2))
			angles.append(np.arctan2(y_length, x_length) * 180 / np.pi)


	passes_df['success'] = successes
	passes_df['distance'] = distances
	passes_df['angle'] = angles


	#p-stwo success given distance
	bins = 50
	range= [0, 100]

	success_df = passes_df[passes_df['success']==True]
	succ_hist = np.histogram(np.floor(success_df['distance']), bins=bins, range=range)
	pass_hist = np.histogram(np.floor(passes_df['distance']), bins=bins, range=range)

	prob_sucess = np.divide(succ_hist[0], pass_hist[0])
	plt.scatter(succ_hist[1][:-1], prob_sucess, label='datapoints')

	x = np.arange(100, step=0.5)
	plt.plot(x, logifunc(x, 0.8, 80, -0.08,0), label='My model')

	popt,pcov = curve_fit(logifunc,succ_hist[1][:-1], prob_sucess, p0=[1, 50, -0.07,0])
	print(popt)
	plt.plot(x, logifunc(x, *popt), 'r-', label='Fitted function scipy')



	plt.legend()
	plt.title('Probability of successful pass given distance')
	plt.xlabel('Distance')
	plt.ylabel('Probability')
	plt.show()

	print('slut')




def logifunc(x, A, x0, k, off):
	return A / (1 + np.exp(-k * (x - x0))) + off














if __name__ == '__main__':
	passes()