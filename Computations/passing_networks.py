import json

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from Computations import pitch

base_url = 'D:\statsbomb\open-data\data\events\{}.json'
game_id = 3795221  # England vs Danmark
team_name = 'Denmark'
passes_cutoff = 5
transparency_factor = 90

# team_name = 'England'
# passes_cutoff = 5
# transparency_factor = 90

lineup_ids = []
id_to_number_mapping = {}
color_players = 'blue'
player_size_factor = 2
pass_width_factor = 0.22


def passing_networks():
	with open(base_url.format(game_id)) as f:
		data = json.load(f)

	df = pd.DataFrame(data)
	passes_df = pd.DataFrame(
		columns=['player_id', 'player_name', 'recipient_id', 'recipient_name', 'location', 'location_end', 'length',
				 'angle'])

	# Create new passes df
	for i, event in df.iterrows():
		# initialize lineup_ids
		if event['type']['name'] == 'Starting XI' and event['team']['name'] == team_name:
			for player in event['tactics']['lineup']:
				lineup_ids.append(player['player']['id'])
				id_to_number_mapping[player['player']['id']] = player['jersey_number']

		if event['type']['name'] != 'Pass':
			continue

		if event['team']['name'] != team_name:
			continue

		# passes only between players from starting eleven

		# no recipient, if pass incomplete. therefore try except clause
		try:
			# ignore passes if one of players was not in tartup XI
			if event['pass']['recipient']['id'] not in lineup_ids or event['player']['id'] not in lineup_ids:
				continue

			new_pass = {
				'recipient_id': event['pass']['recipient']['id'],
				'recipient_name': event['pass']['recipient']['name'],
				'recipient_number': event['pass']['recipient']['name'],
				'player_id': event['player']['id'],
				'player_name': event['player']['name'],
				'player_number': event['player']['name'],
				'location': event['location'],
				'location_end': event['pass']['end_location'],
				'length': event['pass']['length'],
				'angle': event['pass']['angle'],
			}
			passes_df = passes_df.append(new_pass, ignore_index=True)

		except KeyError:
			pass

	# Create dict with players and list of their x and y locations
	player_ids = passes_df['player_id'].drop_duplicates()

	locs = dict()
	for id in player_ids.values:
		locs[id] = {'x': [], 'y': [], 'passes_no': {}, 'number': -1}

	for i, pas in passes_df.iterrows():
		# append player and recipient location to their respective dicts.
		locs[pas['player_id']]['x'].append(pas['location'][0])
		locs[pas['player_id']]['y'].append(pas['location'][1])
		locs[pas['recipient_id']]['x'].append(pas['location_end'][0])
		locs[pas['recipient_id']]['y'].append(pas['location_end'][1])

		# read player number
		# locs[pas['player_id']]['number'] = pas['player']
		# locs[pas['recipient_id']]['number'] =

		# increment number of passes between player and recipient
		try:
			locs[pas['player_id']]['passes_no'][pas['recipient_id']] += 1
		except KeyError:
			locs[pas['player_id']]['passes_no'][pas['recipient_id']] = 1

		try:
			locs[pas['recipient_id']]['passes_no'][pas['player_id']] += 1
		except KeyError:
			locs[pas['recipient_id']]['passes_no'][pas['player_id']] = 1

	# x_avgs = []
	# y_avgs = []
	fig, ax = pitch.plot_pitch()

	## compute and avg position of all players
	for key in locs:
		locs[key]['x_avg'] = np.average(locs[key]['x'])
		locs[key]['y_avg'] = np.average(locs[key]['y'])

	for key in locs:
		# plot avg positions
		x_avg = locs[key]['x_avg']
		y_avg = locs[key]['y_avg']
		# ax.scatter(x_avg, y_avg, s=player_size_factor * len(locs[key]['x']), c=color_players, edgecolor='dark'+color_players,)
		ax.scatter(x_avg, y_avg, s=200, c=color_players, edgecolor='dark' + color_players, )
		ax.text(x_avg, y_avg, id_to_number_mapping[key], ha='center', va='center')

		# plot connections between players if number of passes greater than passes_cutoff
		for p_id in locs[key]['passes_no']:
			no_of_passes = locs[key]['passes_no'][p_id]
			if no_of_passes > passes_cutoff:
				line_xs = [x_avg, locs[p_id]['x_avg']]
				line_ys = [y_avg, locs[p_id]['y_avg']]
				ax.plot(line_xs, line_ys, c=color_players, linewidth=no_of_passes * pass_width_factor, zorder=0,
						alpha=no_of_passes / transparency_factor)

	plt.show()

	print('slut')


if __name__ == '__main__':
	passing_networks()
	# pitch.plot_pitch()
	# plt.show()