import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import statsmodels.api as sm
import statsmodels.formula.api as smf


def regression_tests():
	years_worked = np.arange(10)
	avg_salary = np.array([1000, 2450, 4567, 7565, 8574, 7685, 9876, 10990, 10174, 11463])
	avg_salary_norm = avg_salary/avg_salary.max()

	df = pd.DataFrame()
	df = df.assign(years=years_worked)
	df = df.assign(salary=avg_salary)
	df = df.assign(salary_norm=avg_salary_norm)
	print(df)

	fig, ax = plt.subplots(num=1)
	ax.plot(years_worked, avg_salary, linestyle='none', marker='.', markerSize=12, color='black')
	ax.set_ylabel('Salary')
	ax.set_xlabel('Experience')
	plt.xlim((0, 10))
	plt.ylim((0, 15000))

	est_color = 'blue'
	model_color = 'red'

	b = 2900
	a = 1000
	x = np.arange(10,step=0.1)
	y = a*x + b
	# ax.plot(x, y, color=est_color)

	c = [-1.4,0.6]
	d =10000
	logfun = d/(1 + np.exp(-c[0] - x*c[1]))
	ax.plot(x, logfun, color=est_color)


	model = smf.ols(data=df, formula='avg_salary ~ years_worked ')
	result = model.fit()
	[b,a]= result.params
	# print(result.summary())
	# print(a)
	# print(b)
	# ax.plot(x, a*x + b, color=model_color)

	model2 = smf.logit(data=df, formula='avg_salary_norm ~ years_worked')
	# model2 = smf.glm(data=df, formula='avg_salary_norm ~ years_worked', family=sm.families.Binomial())
	result2 = model2.fit()
	print(result2.summary())
	params2 = result2.params
	ax.plot(x, avg_salary.max()/(1 + np.exp(-params2[0] - x*params2[1])), color=model_color)

	plt.show()


if __name__ == '__main__':
	regression_tests()
