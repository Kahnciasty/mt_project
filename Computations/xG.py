# The basics
import pandas as pd
import numpy as np
import json

# Plotting
import matplotlib.pyplot as plt
# import FCPython

# Statistical fitting of models
import statsmodels.api as sm
import statsmodels.formula.api as smf
from statsmodels.iolib import load_pickle

import MathHelper
from Computations import FCPython
import statsmodels.formula.api as smf

# wyscout eevnt tags - https://support.wyscout.com/matches-wyid-events


def xG():
	# Decide which league to load
	# Wyscout data from https://figshare.com/collections/Soccer_match_event_dataset/4415000/2
	with open('D:\wyscout\wyscout-soccer-match-event-dataset\\raw_data\events\events_World_Cup.json') as f:
	# with open('D:\wyscout\wyscout-soccer-match-event-dataset\\raw_data\events\events_Germany.json') as f:
		data = json.load(f)
	pd.set_option('display.max_columns', None)
	# Create a data set of shots.
	df = pd.DataFrame(data)
	# print(df.iloc[0:5])
	# pd.unique(df['subEventName'])
	shots = df[df['subEventName'] == 'Shot']
	shots_model = pd.DataFrame(columns=['Goal', 'X', 'Y'])

	# print(shots.iloc[34].tags)
	# print(shots.iloc[34:39])

	# goal center position
	gcp = [0, 50]

	# for i, shot in shots.iloc[34:39].iterrows():
	for i, shot in shots.iterrows():
		shots_model.at[i, 'X'] = 100 - shot['positions'][0]['x']
		shots_model.at[i, 'Y'] = shot['positions'][0]['y']
		shots_model.at[i, 'C'] = abs(shot['positions'][0]['y'] - 50)
		shots_model.at[i, 'Goal'] = 0

		# Distance in metres and shot angle in radians.
		x = shots_model.at[i, 'X'] * 105 / 100
		y = shots_model.at[i, 'C'] * 65 / 100

		shots_model.at[i, 'Dist'] = np.sqrt(x ** 2 + y ** 2)

		theta = np.arctan(7.32 * x / (x ** 2 + y ** 2 - (7.32 / 2) ** 2))
		if theta < 0:
			theta = np.pi + theta
		shots_model.at[i, 'Angle'] = theta * 180 / np.pi

		# print(shot.tags)
		for tag in shot['tags']:
			if tag['id'] == 101:
				shots_model.at[i, 'Goal'] = 1

	goals_only_model = shots_model[shots_model['Goal'] == 1]
	# print(goals_only_model)
	# shots_df = df[df['subEventName'] == 'Shot']
	# shots_state_df = pd.DataFrame(columns=['Goal', 'X', 'Y'])

	# Two dimensional histogram
	H_Shot = np.histogram2d(shots_model['X'], shots_model['Y'], bins=50, range=[[0, 100], [0, 100]])
	H_Goal = np.histogram2d(goals_only_model['X'], goals_only_model['Y'], bins=50, range=[[0, 100], [0, 100]])

	# plot_heatmaps(H_Goal, H_Shot)

	# plot_angles(shots_model, goals_only_model)
	# plot_distances(shots_model, goals_only_model)

	general_model(shots_model, goals_only_model)

	plt.show()

	print('slut')


def plot_heatmaps(H_Goal, H_Shot):
	(fig, ax) = FCPython.createGoalMouth()

	# plot shots position
	# pos = ax.imshow(H_Shot[0], extent=[-1, 66, 104, -1], aspect='auto', cmap=plt.cm.Reds)
	# ax.set_title('Shots position')

	# plot goals position
	pos = ax.imshow(H_Goal[0], extent=[-1, 66, 104, -1], aspect='auto', cmap=plt.cm.Reds)
	ax.set_title('Goals position')

	# plot probability of scorin
	# pos = ax.imshow(H_Goal[0] / H_Shot[0], extent=[-1, 66, 104, -1], aspect='auto', cmap=plt.cm.Reds)
	# ax.set_title('Probability of scoring')

	fig.colorbar(pos, ax=ax)
	plt.xlim((-1, 66))
	plt.ylim((-1, 35))
	plt.tight_layout()
	plt.gca().set_aspect('equal', adjustable='box')
	plt.show()


def plot_angles(shots_model, goals_only_model):
	# P-stwo goal given angle
	goals_angle = np.histogram(np.floor(goals_only_model['Angle']), bins=40, range=[0, 150])
	shot_angle = np.histogram(np.floor(shots_model['Angle']), bins=40, range=[0, 150])
	prob_goal = np.divide(goals_angle[0], shot_angle[0])
	fig, ax = plt.subplots(num=1)
	ax.plot(shot_angle[1][:-1], prob_goal, linestyle='none', marker='.', markerSize=12, color='black')
	ax.set_ylabel('Probability chance scored')
	ax.set_xlabel("Shot angle (degrees)")
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)

	b = [-3.5, 0.05]
	x = np.arange(150, step=0.5)
	ax.plot(x, 1 / (1 + np.exp(-b[0] - b[1] * x)))

	print('Beginning to look for model')
	model_angle = smf.glm(data=shots_model, formula='Goal ~ Angle', family=sm.families.Binomial())

	result = model_angle.fit()
	print(result.summary())
	ax.plot(x, 1 / (1 + np.exp(result.params[0] + result.params[1] * x)), color='red')


def plot_distances(shots_model, goals_only_model):
	goals_distance = np.histogram(goals_only_model['Dist'], bins=30, range=[0, 65])
	shot_distance = np.histogram(shots_model['Dist'], bins=30, range=[0, 65])
	prob_goal = np.divide(goals_distance[0], shot_distance[0])

	fig, ax = plt.subplots(num=1)
	ax.plot(goals_distance[1][:-1], prob_goal, linestyle='none', marker='.', markerSize=12, color='black')
	ax.set_ylabel('Probability chance scored')
	ax.set_xlabel('Shot distance')
	ax.spines['top'].set_visible(False)
	ax.spines['right'].set_visible(False)

	b = [0.01, 0.3]
	x = np.arange(65, step=0.2)
	ax.plot(x, 1 / (1 + np.exp(b[0] + b[1] * x)))
	dist_model = smf.glm(formula='Goal ~ Dist', data=shots_model, family=sm.families.Binomial())
	result = dist_model.fit()
	print(result.summary())
	ax.plot(x, 1 / (1 + np.exp(result.params[0] + result.params[1] * x)), color='red')


def general_model(shots_model, goals_only_model):
	variables = ['Dist', 'Angle']
	variable = ' + '.join(variables)
	# xG_model = smf.glm(formula='Goal ~ ' + variable, data=shots_model, family=sm.families.Binomial())
	# result = xG_model.fit()
	# result.save('xG_model_result_dist_angle.pickle')
	# print(result.summary())
	result_loaded = sm.load('xG_model_result_dist_angle.pickle')


	# xG = shots_model.apply(calculate_xG, axis=1)
	# shots_model = shots_model.assign(xG=xG)

	# Create a 2D map of xG
	pgoal_2d = np.zeros((65, 65))
	for x in range(65):
		for y in range(65):
			sh = dict()
			a = np.arctan(7.32 * x / (x ** 2 + abs(y - 65 / 2) ** 2 - (7.32 / 2) ** 2))
			if a < 0:
				a = np.pi + a
			sh['Angle'] = a * 180 / np.pi
			sh['Dist'] = np.sqrt(x ** 2 + abs(y - 65 / 2) ** 2)
			sh['D2'] = x ** 2 + abs(y - 65 / 2) ** 2
			sh['X'] = x
			sh['AX'] = x * a
			sh['X2'] = x ** 2
			sh['Y'] = y
			sh['C'] = abs(y - 65 / 2)
			sh['C2'] = (y - 65 / 2) ** 2

			pgoal_2d[x, y] = calculate_xG(sh, result_loaded.params, variables)

	(fig, ax) = FCPython.createGoalMouth()
	pos = ax.imshow(pgoal_2d, extent=[-1, 65, 65, -1], aspect='auto', cmap=plt.cm.Reds, vmin=0, vmax=0.5)
	fig.colorbar(pos, ax=ax)
	ax.set_title('xG')
	plt.xlim((0, 66))
	plt.ylim((-3, 35))
	plt.gca().set_aspect('equal', adjustable='box')


# Return xG value for more general model
def calculate_xG(sh, params, variables):
	bsum = params[0]
	for i, v in enumerate(variables):
		bsum = bsum + params[i + 1] * sh[v]
	xG = 1 / (1 + np.exp(bsum))
	return xG


if __name__ == '__main__':
	xG()
