from matplotlib import pyplot as plt
import numpy as np


def plot_pitch(length=120, width=80):
	fig, ax = plt.subplots()
	color_line = 'black'
	zorder = -1

	# All 'square' figures drawing start at bottom left corner and goes anti clockwise. Start pont and end point are the same

	# plot pitch border
	ax.plot([0, length, length, 0, 0], [0, 0, width, width, 0], c=color_line, zorder=zorder)

	# plot middle line
	ax.plot([length / 2, length / 2], [0, width], c=color_line, zorder=zorder)

	# plot middle point
	ax.scatter([length / 2], [width / 2], s=20, c=color_line, zorder=zorder)

	# left penalty box
	penalty_box_y = 40.3
	penalty_box_x = 16.5
	ax.plot([0, penalty_box_x, penalty_box_x, 0, 0],
			[(width - penalty_box_y) / 2, (width - penalty_box_y) / 2, (width + penalty_box_y) / 2,
			 (width + penalty_box_y) / 2, (width - penalty_box_y) / 2], c=color_line, zorder=zorder)

	# right penalty box
	ax.plot([length - penalty_box_x, length, length, length - penalty_box_x, length - penalty_box_x],
			[(width - penalty_box_y) / 2, (width - penalty_box_y) / 2, (width + penalty_box_y) / 2,
			 (width + penalty_box_y) / 2, (width - penalty_box_y) / 2], c=color_line, zorder=zorder)

	# left goalie box
	goalie_box_x = 5.5
	goalie_box_y = 18.3
	ax.plot([0, goalie_box_x, goalie_box_x, 0, 0],
			[(width - goalie_box_y) / 2, (width - goalie_box_y) / 2, (width + goalie_box_y) / 2,
			 (width + goalie_box_y) / 2, (width - goalie_box_y) / 2], c=color_line, zorder=zorder)

	# right goalie box
	ax.plot([length - goalie_box_x, length, length, length - goalie_box_x, length - goalie_box_x],
			[(width - goalie_box_y) / 2, (width - goalie_box_y) / 2, (width + goalie_box_y) / 2,
			 (width + goalie_box_y) / 2, (width - goalie_box_y) / 2], c=color_line, zorder=zorder)

	# left goal
	goal_x = 3
	goal_y = 7.32
	ax.plot([-goal_x, 0, 0, -goal_x, -goal_x],
			[(width - goal_y) / 2, (width - goal_y) / 2, (width + goal_y) / 2,
			 (width + goal_y) / 2, (width - goal_y) / 2], c=color_line, zorder=zorder)

	# right goal
	ax.plot([length, length + goal_x, length + goal_x, length, length],
			[(width - goal_y) / 2, (width - goal_y) / 2, (width + goal_y) / 2,
			 (width + goal_y) / 2, (width - goal_y) / 2], c=color_line, zorder=zorder)

	# left penalty spot
	ax.scatter([11], [width / 2], s=15, c=color_line, zorder=zorder)

	# right penalty spot
	ax.scatter([length - 11], [width / 2], s=15, c=color_line, zorder=zorder)

	# plot middle circle
	phis = np.arange(0, 2*np.pi, 0.01)
	r = 9.15
	x, y = xy(r, phis)
	x += length /2
	y += width /2
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	# plot left penalty arc
	phis = np.arange(-0.92, 0.92, 0.01)
	r = 9.15
	x, y = xy(r, phis)
	x += 11
	y += width /2
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	# plot right penalty arc
	phis = np.arange(-0.92 + np.pi, 0.92 + np.pi, 0.01)
	r = 9.15
	x, y = xy(r, phis)
	x += length - 11
	y += width /2
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	# plot bottom left corner
	phis = np.arange(0,np.pi/2, 0.01)
	r = 2
	x, y = xy(r, phis)
	x += 0
	y += 0
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	# plot bottom right corner
	phis = np.arange(np.pi/2, np.pi, 0.01)
	r = 2
	x, y = xy(r, phis)
	x += length
	y += 0
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	# plot top right corner
	phis = np.arange(np.pi, 3/2*np.pi, 0.01)
	r = 2
	x, y = xy(r, phis)
	x += length
	y += width
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	# plot top left corner
	phis = np.arange(3/2*np.pi, 2* np.pi, 0.01)
	r = 2
	x, y = xy(r, phis)
	x += 0
	y += width
	ax.plot(x, y, c=color_line, ls='-', zorder=zorder)

	return fig, ax


def xy(r, phi):
	return r * np.cos(phi), r * np.sin(phi)
