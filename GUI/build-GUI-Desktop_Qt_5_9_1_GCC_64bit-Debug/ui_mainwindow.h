/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *canvasLayout;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *tenSecBack;
    QPushButton *fiveSecBack;
    QPushButton *pauseButton;
    QPushButton *fiveSecForward;
    QPushButton *tenSecForward;
    QListWidget *eventListWidget;
    QCheckBox *eventCheckBox;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QToolBar *toolBar;
    QToolBar *toolBar_2;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1694, 961);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setStyleSheet(QLatin1String("/*\n"
"QPushButton, QListWidget{\n"
"            margin: 1px;\n"
"            border-color: #0c457e;\n"
"            border-style: outset;\n"
"            border-radius: 3px;\n"
"           border-width: 1px;\n"
"           color: black;\n"
"min-height:24;\n"
"        }"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"\n"
"\n"
""));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 0, 1691, 901));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        canvasLayout = new QVBoxLayout();
        canvasLayout->setSpacing(6);
        canvasLayout->setObjectName(QStringLiteral("canvasLayout"));

        horizontalLayout->addLayout(canvasLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(6, 6, 6, 6);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(5, 5, 5, 5);
        tenSecBack = new QPushButton(horizontalLayoutWidget);
        tenSecBack->setObjectName(QStringLiteral("tenSecBack"));
        tenSecBack->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_3->addWidget(tenSecBack);

        fiveSecBack = new QPushButton(horizontalLayoutWidget);
        fiveSecBack->setObjectName(QStringLiteral("fiveSecBack"));
        fiveSecBack->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_3->addWidget(fiveSecBack);

        pauseButton = new QPushButton(horizontalLayoutWidget);
        pauseButton->setObjectName(QStringLiteral("pauseButton"));
        pauseButton->setMaximumSize(QSize(60, 16777215));
        pauseButton->setFocusPolicy(Qt::NoFocus);

        horizontalLayout_3->addWidget(pauseButton);

        fiveSecForward = new QPushButton(horizontalLayoutWidget);
        fiveSecForward->setObjectName(QStringLiteral("fiveSecForward"));
        fiveSecForward->setMaximumSize(QSize(60, 16777215));

        horizontalLayout_3->addWidget(fiveSecForward);

        tenSecForward = new QPushButton(horizontalLayoutWidget);
        tenSecForward->setObjectName(QStringLiteral("tenSecForward"));

        horizontalLayout_3->addWidget(tenSecForward);


        verticalLayout_2->addLayout(horizontalLayout_3);

        eventListWidget = new QListWidget(horizontalLayoutWidget);
        eventListWidget->setObjectName(QStringLiteral("eventListWidget"));
        eventListWidget->setMaximumSize(QSize(300, 16777215));
        eventListWidget->setFocusPolicy(Qt::NoFocus);
        eventListWidget->setLayoutDirection(Qt::LeftToRight);
        eventListWidget->setAutoFillBackground(false);
        eventListWidget->setStyleSheet(QStringLiteral(""));
        eventListWidget->setAutoScroll(false);
        eventListWidget->setSelectionMode(QAbstractItemView::SingleSelection);

        verticalLayout_2->addWidget(eventListWidget);

        eventCheckBox = new QCheckBox(horizontalLayoutWidget);
        eventCheckBox->setObjectName(QStringLiteral("eventCheckBox"));
        eventCheckBox->setFocusPolicy(Qt::NoFocus);

        verticalLayout_2->addWidget(eventCheckBox);


        horizontalLayout->addLayout(verticalLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1694, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);
        toolBar = new QToolBar(MainWindow);
        toolBar->setObjectName(QStringLiteral("toolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar);
        toolBar_2 = new QToolBar(MainWindow);
        toolBar_2->setObjectName(QStringLiteral("toolBar_2"));
        MainWindow->addToolBar(Qt::TopToolBarArea, toolBar_2);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        tenSecBack->setText(QApplication::translate("MainWindow", "<<", Q_NULLPTR));
        fiveSecBack->setText(QApplication::translate("MainWindow", "<", Q_NULLPTR));
        pauseButton->setText(QApplication::translate("MainWindow", "||", Q_NULLPTR));
        fiveSecForward->setText(QApplication::translate("MainWindow", ">", Q_NULLPTR));
        tenSecForward->setText(QApplication::translate("MainWindow", ">>", Q_NULLPTR));
        eventCheckBox->setText(QApplication::translate("MainWindow", "Show original event location", Q_NULLPTR));
        toolBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", Q_NULLPTR));
        toolBar_2->setWindowTitle(QApplication::translate("MainWindow", "toolBar_2", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
