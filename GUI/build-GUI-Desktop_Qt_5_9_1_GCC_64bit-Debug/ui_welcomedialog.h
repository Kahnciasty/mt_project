/********************************************************************************
** Form generated from reading UI file 'welcomedialog.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WELCOMEDIALOG_H
#define UI_WELCOMEDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Dialog
{
public:
    QDialogButtonBox *buttonBox;
    QPushButton *loadButton;
    QLabel *gameLabel;
    QProgressBar *progressBar;

    void setupUi(QDialog *Dialog)
    {
        if (Dialog->objectName().isEmpty())
            Dialog->setObjectName(QStringLiteral("Dialog"));
        Dialog->resize(400, 343);
        Dialog->setStyleSheet(QLatin1String("/*background-color: rgb(255, 255, 255);\n"
"\n"
"QPushButton, QDialogButtonBox:Ok {\n"
"            margin: 1px;\n"
"			background-color:#cafbfc;\n"
"            border-color: #0c457e;\n"
"            border-style: outset;\n"
"            border-radius: 3px;\n"
"           border-width: 1px;\n"
"           color: black;\n"
"        }"));
        buttonBox = new QDialogButtonBox(Dialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(30, 280, 341, 32));
        buttonBox->setStyleSheet(QLatin1String("/*\n"
"QPushButton {\n"
"            margin: 1px;\n"
"            border-color: #0c457e;\n"
"            border-style: outset;\n"
"            border-radius: 3px;\n"
"           border-width: 1px;\n"
"           color: black;\n"
"min-width:70;\n"
"min-height:24;\n"
"        }"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        loadButton = new QPushButton(Dialog);
        loadButton->setObjectName(QStringLiteral("loadButton"));
        loadButton->setGeometry(QRect(100, 30, 211, 31));
        loadButton->setFocusPolicy(Qt::NoFocus);
        loadButton->setStyleSheet(QLatin1String("/*\n"
"\n"
"QPushButton {\n"
"            margin: 1px;\n"
"            border-color: #0c457e;\n"
"            border-style: outset;\n"
"            border-radius: 3px;\n"
"           border-width: 1px;\n"
"           color: black;\n"
"        }"));
        gameLabel = new QLabel(Dialog);
        gameLabel->setObjectName(QStringLiteral("gameLabel"));
        gameLabel->setGeometry(QRect(0, 90, 401, 141));
        QFont font;
        font.setPointSize(12);
        gameLabel->setFont(font);
        gameLabel->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        progressBar = new QProgressBar(Dialog);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(117, 250, 171, 23));
        progressBar->setStyleSheet(QLatin1String("/*QProgressBar ,QProgressBar::chunk{\n"
"    text-align: center;\n"
"           margin: 1px;\n"
"            border-color: #0c457e;\n"
"            border-style: outset;\n"
"            border-radius: 3px;\n"
"           border-width: 1px;\n"
"           color: black;\n"
"			background:#0c457e;\n"
"			background-color:white;\n"
"}"));
        progressBar->setValue(24);

        retranslateUi(Dialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), Dialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), Dialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(Dialog);
    } // setupUi

    void retranslateUi(QDialog *Dialog)
    {
        Dialog->setWindowTitle(QApplication::translate("Dialog", "Dialog", Q_NULLPTR));
        loadButton->setText(QApplication::translate("Dialog", "Select Game", Q_NULLPTR));
        gameLabel->setText(QApplication::translate("Dialog", "No game selected", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Dialog: public Ui_Dialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WELCOMEDIALOG_H
