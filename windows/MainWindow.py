import time
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import Qt
from PyQt4.QtGui import QMainWindow, QIcon, QItemSelectionModel,QMessageBox
from matplotlib.backends.backend_qt4agg import FigureCanvas

import Constants
import MathHelper
from sequence.Aligner import Aligner
from typedefs.EventType import EventType
from GUI.GUI.MainWindow import Ui_MainWindow
from windows.GameViewer import GameViewer
from typedefs.TeamType import TeamType
from my_xml.XmlWriter import XmlWriter


class MainWindow(QMainWindow):
	def __init__(self, dir_path, game_id, game):
		super(MainWindow, self).__init__()
		self.ui = Ui_MainWindow()
		self.ui.setupUi(self)
		self.game_id = game_id
		self.dir_path = dir_path
		self.game = game

		self.xmlWriter = XmlWriter(self.dir_path, game)

		self.viewer = GameViewer(self.game)
		self.viewer.ui_window = self

		self.set_title()

		self.figure = self.viewer.fig
		self.canvas = FigureCanvas(self.figure)
		self.canvas.setVisible(False)
		self.ui.canvasLayout.addWidget(self.canvas)
		self.initialize_widgets()

		# self.overlay = Overlay(self.ui.eventListWidget)
		# self.overlay.hide()
		self.show_initial_frame()

		# self.setFixedSize(self.size())

		self.keyPressEvent = self.newOnkeyPressEvent

		self.sync_task = TaskThread(self.viewer, self.game, self.xmlWriter, self.dir_path, self.game_id)
		self.sync_task.taskFinished.connect(self.finish_syncing)

		self.filtered_events_main = self.viewer.game.events
		self.filtered_events_stats = self.viewer.game.events

		self.marker_size = 40
		self.event_id_column = 6

	# self.sync_task.task.connect(self.hide_sync_progress_bar)

	# self.pause_button_clicked()

	# self.pushButton.clicked.connect(clicked)
	def set_title(self):
		self.game.load_game_summary()
		summary = self.game.game_summary
		self.setWindowTitle("ANOTHER TITLE")
		season = "{0} {1}".format(summary.competition_name, summary.season_name)
		matchday = "Matchday {0}".format(summary.matchday)
		result = "{0} {1} vs. {2} {3}".format(summary.home_team_name, summary.home_score, summary.away_score,
											  summary.away_team_name)
		date = summary.game_date
		self.setWindowTitle("{0} {1} {2}, {3}".format(season, matchday, date, result))

	def initialize_widgets(self):
		self.ui.pauseButton.clicked.connect(self.pause_button_clicked)
		self.initialize_table_view(self.viewer.game.events, self.ui.eventTableView)
		self.initialize_table_view(self.viewer.game.events, self.ui.eventStatsTableView)
		self.initialize_table_view(self.viewer.game.events, self.ui.displayedEventsTableView)
		self.ui.eventCheckBox.clicked.connect(self.event_checkbox_clicked)
		# self.ui.slowerButton.clicked.connect(self.slower_button_clicked)
		# self.ui.fasterButton.clicked.connect(self.faster_button_clicked)
		self.ui.tenSecBack.clicked.connect(self.ten_sec_back_clicked)
		self.ui.fiveSecBack.clicked.connect(self.five_sec_back_clicked)
		self.ui.fiveSecForward.clicked.connect(self.five_sec_forward_clicked)
		self.ui.tenSecForward.clicked.connect(self.ten_sec_forward_clicked)
		self.ui.trainingButton.clicked.connect(self.training_clicked)
		self.ui.syncButton.clicked.connect(self.sync_button_clicked)
		self.ui.applyStatsFilterButton.clicked.connect(self.apply_stats_filter_clicked)
		self.ui.homePassNetwork.clicked.connect(self.home_pass_network_clicked)
		self.ui.awayPassNetwork.clicked.connect(self.away_pass_network_clicked)

		self.ui.eventTableView.clicked.connect(self.event_table_view_clicked)
		self.ui.eventStatsTableView.clicked.connect(self.event_stats_table_view_clicked)
		self.ui.displayedEventsTableView.clicked.connect(self.displayed_events_table_view_clicked)
		self.ui.displayedEventsTableView.selectionModel().selectionChanged.connect(
			self.displayed_events_table_view_clicked)

		#Sync/Async buttons do not have loading bar included. It may take a while to load this data, and in this time the app may be unresponsive
		self.ui.useSyncButton.clicked.connect(self.use_sync_data)
		self.ui.useAsyncButton.clicked.connect(self.use_async_data)
		# self.ui.useAsyncButton.setVisible(False)
		# self.ui.useSyncButton.setVisible(False)

		self.ui.syncProgressBar.setVisible(False)
		self.ui.sync_label.setVisible(False)

		self.ui.receptionCheckBox.clicked.connect(self.reception_checkbox_clicked)

		self.ui.mainTab.setLayout(self.ui.mainTabLayout)
		self.ui.statsTab.setLayout(self.ui.statsTabLayout)
		self.ui.syncTab.setLayout(self.ui.syncTabLayout)

		self.initialize_filter_comboboxes()
		self.initialize_filter_comboboxes_selection_change()

		# self.ui.eventCheckBox.setVisible(False)

		self.initialize_max_minute_spin_box()
		self.ui.eventArrowsCheckBox.clicked.connect(self.event_arrow_check_box_clicked)
		self.ui.penaltyCheckBox.clicked.connect(self.penalty_checkbox_clicked)

		self.ui.trainingButton.setVisible(False)

		self.ui.awayPassNetwork.setText(
			"{0} passing network".format(self.game.get_team_by_team_type(TeamType.AWAY).name))
		self.ui.homePassNetwork.setText(
			"{0} passing network".format(self.game.get_team_by_team_type(TeamType.HOME).name))

		self.ui.eventStatsTableView.setStyleSheet(
			"QTableView { selection-background-color: #0078fa; selection-color: white;}")
		self.ui.displayedEventsTableView.setStyleSheet(
			"QTableView { selection-background-color: #0078fa; selection-color: white;}")

		self.ui.pushButton_1.setVisible(True)
		self.ui.pushButton_1.setText('Expected Goals - xG')
		self.ui.pushButton_1.clicked.connect(self.xG_clicked)

		self.ui.pushButton_2.setVisible(True)
		self.ui.pushButton_2.setText('My index')
		self.ui.pushButton_2.clicked.connect(self.my_index_clicked)

		self.ui.pushButton_3.setVisible(True)
		self.ui.pushButton_3.setText('Draw Players')
		self.ui.pushButton_3.clicked.connect(self.draw_players_clicked)

		self.ui.pushButton_4.setVisible(False)
		self.ui.pushButton_5.setVisible(False)
		self.ui.pushButton_6.setVisible(False)

		self.ui.pitchControlButton.clicked.connect(self.pitch_control_clicked)

	def initialize_table_view(self, events, table_view):
		model = QtGui.QStandardItemModel(len(events), 6)
		model.setHorizontalHeaderLabels(['', 'Time', '', 'Event', 'Player', 'Team', 'Event ID'])
		for row, event in enumerate(events):
			# mismatch column
			item = self.create_icon_item(event)
			model.setItem(row, 0, item)

			# time column
			model.setItem(row, 1, QtGui.QStandardItem("{0:02d}:{1:02d}".format(event.min, event.sec)))

			# icon column
			item = self.prepare_icon_column(event)

			model.setItem(row, 2, item)

			# player column
			model.setItem(row, 3, QtGui.QStandardItem(str(event.type)))
			if event.player is not None:
				model.setItem(row, 4,
							  QtGui.QStandardItem("{0} {1}".format(str(event.player.last_name), event.player.number)))

			# team column
			team_item = QtGui.QStandardItem(str(self.game.get_team_by_id(event.team_id)))
			# team_item.setBackground(self.get_color_by_team(team))
			model.setItem(row, 5, team_item)

			# event column
			self.event_id_column = 6
			model.setItem(row, self.event_id_column, QtGui.QStandardItem(str(event.id)))

		table_view.setModel(model)
		# I don't need to show event Id column, but I need to include it in the tableview. I use the id to detect which specific event was selected by a user
		# table_view.setColumnHidden(self.event_id_column,True)

		table_view.setColumnWidth(0, 23)  # mismatch column width
		table_view.setColumnWidth(1, 50)  # time column width
		table_view.setColumnWidth(2, 23)  # packing rate column width

	def create_icon_item(self, event):
		item = QtGui.QStandardItem()
		if event.mismatched and not event.player_corrected:
			item.setIcon(QIcon("resource/cross.png"))
			item.setToolTip("Mismatched Event")
		if event.player_corrected:
			item.setIcon(QIcon("resource/tick.jpg"))
			item.setToolTip("Player Corrected")
		if event.type == EventType.GOAL:
			item.setIcon(QIcon("resource/football.jpg"))
			item.setToolTip("Goal")
		return item

	def prepare_icon_column(self, event):
		team = self.game.get_team_by_id(event.team_id)
		item = QtGui.QStandardItem("")
		item.setBackground(self.get_color_by_team(team))
		item.setTextAlignment(QtCore.Qt.AlignRight)
		if event.type == EventType.PASS:
			item.setToolTip("Missed Pass")
			item.setIcon(QIcon("resource/x.png"))
			pack_rate = event.get_packing_rate()
			if pack_rate is not None:
				icon_name = str(pack_rate)
				tooltip = "Packing Rate: {0}".format(icon_name)
				item.setIcon(QIcon("resource/pack_gradient/{0}.png".format(icon_name)))
				item.setToolTip(tooltip)
		elif event.type == EventType.RECEPTION:  # e ⟨0; 3), ⟨3; 6), ⟨6; 9), ⟨9; ∞) [m].
			dist = float(event.get_closest_opponent_distance())
			tooltip = "Distance To Closest Opponent: {0:.2f}[m]".format(dist)
			dist = min(dist / 3, 3)
			icon_name = int(dist)  # 0, 1, 2 or 3

			item.setIcon(QIcon("resource/reception_distance/{0}.png".format(icon_name)))
			item.setToolTip(tooltip)
		elif event.type == EventType.CARRY:  # ⟨0; 4), ⟨4; 8), ⟨8; 12), ⟨12; ∞) [m]
			dist = MathHelper.dist_between(event.coord, event.coord_end) / 100
			tooltip = "Distance Covered With Ball: {0:.2f}[m]".format(dist)
			dist = min(dist / 4, 2)
			icon_name = int(dist)  # 0, 1, 2

			item.setIcon(QIcon("resource/carry_distance/{0}.png".format(icon_name)))
			item.setToolTip(tooltip)

		return item

	def get_color_by_team(self, team):
		if team.team_type == TeamType.AWAY:
			return QtGui.QColor(self.viewer.away_color)
		else:
			return QtGui.QColor(self.viewer.home_color)

	def event_table_view_clicked(self, item):
		event = self.get_event_from_item(item, self.ui.eventTableView)
		if event.is_aligned():
			self.viewer.show_frame_by_frame_id(event.frame_id)

		self.select_event(event)

	def get_event_from_item(self, item, table_view):
		model = table_view.model()
		event_id_cell = model.item(item.row(), self.event_id_column)
		id = event_id_cell.text()
		event = self.game.get_event_by_event_id(id)
		return event

	def show_initial_frame(self):
		if self.viewer.ax is not None:
			self.viewer.ax.clear()
		# ax.clear()
		self.viewer.show()
		self.canvas.draw()
		self.canvas.setVisible(True)

	def pause_button_clicked(self):
		# self.viewer.forward = not self.viewer.forward
		self.viewer.start_or_pause_animation()
		if self.viewer.running:
			self.ui.pauseButton.setText("||")
			self.viewer.reset_pitch()  # reset pitch to get rid of the static events drawn on the pitch before animation start
		else:
			self.ui.pauseButton.setText("▶")

	def select_events(self, events):
		for event in events:
			self.select_event(event)

	def event_stats_table_view_clicked(self, item):
		event = self.get_event_from_item(item, self.ui.eventStatsTableView)

		# select item in display view
		model = self.ui.displayedEventsTableView.model()
		items = model.findItems(event.id, flags=Qt.MatchExactly, column=self.event_id_column)
		for item in items:
			self.ui.displayedEventsTableView.selectionModel().select(item.index(),
																	 QItemSelectionModel.ClearAndSelect | QItemSelectionModel.Rows | QItemSelectionModel.Toggle)

		# scroll to selected item in display view
		sel_model = self.ui.displayedEventsTableView.selectionModel()
		selections = sel_model.selectedRows()
		for selection in selections:
			self.ui.displayedEventsTableView.scrollTo(selection,
													  QtGui.QAbstractItemView.PositionAtCenter)

	def initialize_filter_comboboxes(self):
		self.ui.teamComboBox.addItem("No Team Filter")
		self.ui.teamStatsComboBox.addItem("No Team Filter")

		for team in self.viewer.game.teams:
			self.ui.teamComboBox.addItem(team.name)
			self.ui.teamStatsComboBox.addItem(team.name)

		self.ui.eventComboBox.addItem("No Event Filter")
		self.ui.eventStatsComboBox.addItem("No Event Filter")
		event_type_list = []
		for event in EventType:
			event_type_list.append(event.name)
		event_type_list.sort()
		self.ui.eventComboBox.addItems(event_type_list)
		self.ui.eventStatsComboBox.addItems(event_type_list)

		self.ui.playerStatsComboBox.addItem("No Player Filter")
		for player in self.viewer.game.players:
			self.ui.playerStatsComboBox.addItem(str(player))

	def initialize_filter_comboboxes_selection_change(self):
		self.ui.teamComboBox.currentIndexChanged.connect(self.main_filter_selection_change)
		self.ui.eventComboBox.currentIndexChanged.connect(self.main_filter_selection_change)

	# self.ui.teamStatsComboBox.currentIndexChanged.connect(self.stats_filter_selection_change)
	# self.ui.eventStatsComboBox.currentIndexChanged.connect(self.stats_filter_selection_change)
	# self.ui.playerStatsComboBox.currentIndexChanged.connect(self.stats_filter_selection_change)

	def main_filter_selection_change(self):
		filtered_events = self.filter_selection_change(self.ui.eventTableView, self.ui.teamComboBox,
													   self.ui.eventComboBox)
		self.filtered_events_main = filtered_events

	def stats_filter_selection_change(self):
		# filtered_events = self.filter_selection_change(self.ui.eventStatsListWidget, self.ui.teamStatsComboBox,
		# 											   self.ui.eventStatsComboBox, self.ui.playerStatsComboBox)
		# self.filtered_events_stats = filtered_events
		# self.draw_events(self.filtered_events_stats)
		pass

	def draw_heatmap(self, events):
		self.viewer.pause_animation()
		#uncomment all coments to clear the pitch before drawing the event location
		# self.viewer.reset_pitch()
		# self.ui.canvasLayout.removeWidget(self.canvas)
		# self.viewer.reset_pitch()

		# away_colors, home_colors = self.prepare_gradients(events)
		show_text = self.ui.showEventTextOnPitchCheckBox.isChecked()
		self.viewer.draw_static_events(events, show_text)

		# self.canvas = FigureCanvas(self.viewer.fig)
		# self.add_canvas_to_canvas_layout(self.canvas)

	def determine_heatmap_event_color(self, event):
		team = self.viewer.game.get_team_by_id(event.team_id)
		color = self.viewer.home_edge_color if team.team_type == TeamType.HOME else self.viewer.away_edge_color
		return color

	def filter_selection_change(self, table_view, team_filter, event_filter, player_filter=None):
		team_name = team_filter.currentText()
		team = self.viewer.game.get_team_by_name(team_name)
		try:
			event_type = EventType[event_filter.currentText()]
		except KeyError:
			event_type = None

		# event type and team filters
		if team is not None and event_type is None:
			filtered_events = [event for event in self.viewer.game.events if event.team_id == team.id]
		elif team is None and event_type is not None:
			filtered_events = [event for event in self.viewer.game.events if event.type == event_type]
		elif team is not None and event_type is not None:
			filtered_events = [event for event in self.viewer.game.events if
							   event.type == event_type and event.team_id == team.id]
		else:
			filtered_events = self.viewer.game.events

		if player_filter is not None and player_filter.currentText() != "No Player Filter":
			player_name = player_filter.currentText().split()[0]
			filtered_events = [event for event in filtered_events if event.player is not None and
							   event.player.last_name == player_name]

		# time filters
		max_minute = self.ui.max_minute_spin_box.value()
		min_minute = self.ui.min_minute_spin_box.value()
		filtered_events = [event for event in filtered_events if event.min >= min_minute and event.min <= max_minute]

		# penalty box checkbox
		if self.ui.penaltyCheckBox.isChecked():
			filtered_events = [event for event in filtered_events if
							   event.has_end_coords() and event.ends_in_penalty_box()]

		self.filtered_events_stats = filtered_events
		self.initialize_table_view(filtered_events, table_view)
		return filtered_events

	def newOnkeyPressEvent(self, evt):
		k = evt.key()
		if evt.key() == QtCore.Qt.Key_Left:
			self.backward_seconds(5)
		elif evt.key() == QtCore.Qt.Key_Right:
			self.forward_seconds(5)
			self.viewer.start_or_pause_animation()
			self.viewer.update_plot(self.viewer.i)
			self.viewer.start_or_pause_animation()
		elif evt.key() == QtCore.Qt.Key_J:
			self.backward_seconds(10)
		elif evt.key() == QtCore.Qt.Key_L:
			self.forward_seconds(10)
		elif evt.key() == QtCore.Qt.Key_Space:
			self.pause_button_clicked()
		self.viewer.update_plot(self.viewer.i)

	def event_checkbox_clicked(self):
		check = self.ui.eventCheckBox.isChecked()
		self.viewer.event_location_plot_visible = check

	def penalty_checkbox_clicked(self):
		# self.stats_filter_selection_change()
		pass

	def reception_checkbox_clicked(self):
		if self.ui.receptionCheckBox.isChecked():
			self.viewer.show_reception = True
		else:
			self.viewer.show_reception = False

	def draw_events(self, events):
		events.sort(key=lambda event: event.frame_id)
		self.draw_heatmap(events)
		if self.ui.eventArrowsCheckBox.isChecked():
			self.draw_events_arrows(events)

	def draw_events_arrows(self, events):
		away_colors, home_colors = self.prepare_gradients(events)
		self.viewer.draw_static_events_arrows(events, home_colors, away_colors)

	def prepare_gradients(self, events):
		home_events = [event for event in events if event.team.team_type == TeamType.HOME]
		away_events = [event for event in events if event.team.team_type == TeamType.AWAY]

		# The display is ready to prepare gradients depending on time for static events. I think it doesn't improve
		# the visualisation, so for now I just set the same colors for start and end events from the list
		# away_gradient = MathHelper.linear_gradient("#0571b0", "#92c5de", len(away_events))
		# home_gradient = MathHelper.linear_gradient("#FF002B", "#FF8E61", len(home_events))
		away_gradient = MathHelper.linear_gradient(self.viewer.away_edge_color, self.viewer.away_edge_color,
												   len(away_events))
		home_gradient = MathHelper.linear_gradient(self.viewer.home_edge_color, self.viewer.home_edge_color,
												   len(home_events))
		return away_gradient['hex'], home_gradient['hex']

	def select_event(self, event):
		model = self.ui.eventTableView.model()
		items = model.findItems(event.id, flags=Qt.MatchExactly, column=self.event_id_column)
		for item in items:
			self.ui.eventTableView.selectionModel().select(item.index(),
														   QItemSelectionModel.ClearAndSelect | QItemSelectionModel.Rows)

		if self.ui.autoScrollCheckBox.isChecked():
			selections = self.ui.eventTableView.selectionModel().selectedRows()
			for selection in selections:
				self.ui.eventTableView.scrollTo(selection, QtGui.QAbstractItemView.EnsureVisible)

	#Changed to move forward number of frames, not seconds
	def forward_seconds(self, seconds):
		# self.viewer.i += self.viewer.game.position_frequency * seconds
		self.viewer.i += seconds

		self.viewer.show_frame_by_frame_id(self.game.frames[self.viewer.i].id)

	def backward_seconds(self, seconds):
		# self.viewer.i -= self.viewer.game.position_frequency * seconds
		self.viewer.i -= seconds
		self.viewer.show_frame_by_frame_id(self.game.frames[self.viewer.i].id)

	# def slower_button_clicked(self):
	# 	self.viewer.animation.event_source.interval += 10
	#
	# def faster_button_clicked(self):
	# 	self.viewer.animation.event_source.interval -= 10

	def ten_sec_back_clicked(self):
		self.backward_seconds(25)

	def ten_sec_forward_clicked(self):
		self.forward_seconds(25)

	def five_sec_back_clicked(self):
		self.backward_seconds(5)

	def five_sec_forward_clicked(self):
		self.forward_seconds(5)

	def home_pass_network_clicked(self):
		self.compute_passing_network(TeamType.HOME)

	def away_pass_network_clicked(self):
		self.compute_passing_network(TeamType.AWAY)

	def sync_button_clicked(self):
		self.viewer.pause_animation()
		self.ui.sync_label.setVisible(True)
		self.ui.syncProgressBar.setVisible(True)
		self.sync_task.start()

	def training_clicked(self):
		self.viewer.pause_animation()
		self.ui.canvasLayout.removeWidget(self.canvas)

		z = self.viewer.ax.scatter(100, 200, c="yellow")
		self.canvas = FigureCanvas(self.viewer.fig)

		self.add_canvas_to_canvas_layout(self.canvas)

	def add_canvas_to_canvas_layout(self, canvas):
		self.ui.canvasLayout.addWidget(canvas)
		self.viewer.setup_score_board()
		self.viewer.update_score_board(self.game.frames[self.viewer.i])

	def initialize_max_minute_spin_box(self):
		list = [event for event in self.game.events if event.type == EventType.END and event.period_id == 2]
		max = list[0].min  # list.min is a minute when the event happened
		self.ui.max_minute_spin_box.setMaximum(max)
		self.ui.max_minute_spin_box.setValue(max)

		self.ui.max_minute_spin_box.valueChanged.connect(self.spin_box_value_change)
		self.ui.min_minute_spin_box.valueChanged.connect(self.spin_box_value_change)

	def spin_box_value_change(self):
		# filtered_events = self.filter_selection_change(self.ui.eventStatsListWidget, self.ui.teamStatsComboBox,
		# 											   self.ui.eventStatsComboBox)

		# max_minute = self.ui.max_minute_spin_box.value()
		# min_minute = self.ui.min_minute_spin_box.value()
		# filtered_events = [event for event in self.filtered_events_stats if
		# 				   event.min >= min_minute and event.min <= max_minute]
		# self.filtered_events_stats = filtered_events
		# self.draw_events(filtered_events)
		# self.stats_filter_selection_change()
		pass

	def event_arrow_check_box_clicked(self):
		# filtered_events = self.filter_selection_change(self.ui.eventStatsListWidget, self.ui.teamStatsComboBox,
		# 											   self.ui.eventStatsComboBox)
		self.draw_events(self.filtered_events_stats)

	def use_sync_data(self):
		self.viewer.game.load_location_data(use_async=False)

	def use_async_data(self):
		self.viewer.game.load_location_data(use_async=True)

	def handle_stats_item_entered(self):
		print("Entered")

	def handle_stats_item_exited(self):
		print("Exited")

	def apply_stats_filter_clicked(self):
		filtered_events = self.filter_selection_change(self.ui.eventStatsTableView, self.ui.teamStatsComboBox,
													   self.ui.eventStatsComboBox, self.ui.playerStatsComboBox)

	# self.draw_events(filtered_events)

	def displayed_events_table_view_clicked(self):
		sel_model = self.ui.displayedEventsTableView.selectionModel()
		selections = sel_model.selectedRows()
		events = []
		for selection in selections:
			event = self.get_event_from_item(selection, self.ui.displayedEventsTableView)
			events.append(event)

		self.draw_events(events)
		# time.sleep(5)
		if self.ui.playersLocationCheckBox.isChecked() and events[-1].is_aligned():
			self.viewer.show_frame_by_frame_id(events[-1].frame_id)

	def compute_passing_network(self, teamtype):
		network_players = self.game.compute_passing_network(teamtype)
		if not network_players:
			return

		self.draw_passing_network(network_players)

	def draw_passing_network(self, network_players):
		self.viewer.pause_animation()
		self.viewer.reset_pitch()
		self.ui.canvasLayout.removeWidget(self.canvas)
		self.viewer.reset_pitch()

		# self.viewer.draw_static_events(events, home_colors, away_colors, show_text)
		self.viewer.draw_passing_network(network_players)

		self.canvas = FigureCanvas(self.viewer.fig)
		self.add_canvas_to_canvas_layout(self.canvas)

	def finish_syncing(self):
		self.ui.sync_label.setVisible(False)
		self.ui.syncProgressBar.setVisible(False)

		self.initialize_table_view(self.viewer.game.events, self.ui.eventTableView)
		self.initialize_table_view(self.viewer.game.events, self.ui.eventStatsTableView)
		self.initialize_table_view(self.viewer.game.events, self.ui.displayedEventsTableView)

		short_summary, long_summary = self.prepare_alignment_summary()
		self.export_summary_to_file(long_summary)

		short_summary.append("A list of incorrect events can be found in the original files' location")

		self.show_modal(QMessageBox.Information, "Alignment summary", "Alignment summary",
						"".join(short_summary))

		self.pause_button_clicked()

	def show_modal(self, icon, window_title, text, informative_text):
		msg = QMessageBox()
		msg.setIcon(icon)
		msg.setText(text)
		msg.setInformativeText(informative_text)
		msg.setWindowTitle(window_title)
		retval = msg.exec_()

	def prepare_alignment_summary(self):
		original_events = [event for event in self.game.events if
						   event.type != EventType.CARRY and event.type != EventType.RECEPTION]
		added_events = [event for event in self.game.events if
						event.type == EventType.CARRY or event.type == EventType.RECEPTION]

		adjusted = 0
		mismatched = 0
		for event in original_events:
			if event.frame_id is not None:
				adjusted += 1
			if event.mismatched:
				mismatched += 1

		long_summary = []
		short_summary = []

		long_summary.append("Adjusted {0} events which is {1:.2f}% of all {2} original events.\n".format(adjusted,
																										 adjusted / len(original_events) * 100,
																										 len(original_events)))
		print(long_summary[-1])
		short_summary.append(long_summary[-1])

		long_summary.append("Added {0} new events (Receptions and Carries).\n".format(len(added_events)))
		print(long_summary[-1])
		short_summary.append(long_summary[-1])

		long_summary.append("{0} incorrect events. Which is {1:.2f}% of total number of original events.\n".format(mismatched,
																												   mismatched / len(
																													   original_events) * 100))
		print(long_summary[-1])
		short_summary.append(long_summary[-1])
		ids = []
		for event in self.game.aligning_stats.mismatched_events:
			ids.append(event.id)
		ids_string = ", ".join(ids)
		mismatched_ids = "Incorrect events ids: {0}\n".format(ids_string)
		long_summary.append(mismatched_ids)

		long_summary.append("Out of {1} incorrect events, {0} were corrected (changed performer). \n".format(
			len(self.game.aligning_stats.corrected_performers), mismatched))
		print(long_summary[-1])
		short_summary.append(long_summary[-1])

		ids = []
		for event in self.game.aligning_stats.corrected_performers:
			ids.append(event.id)
		ids_string = ", ".join(ids)
		corrected_ids = "Corrected events ids: {0}\n".format(ids_string)
		long_summary.append(corrected_ids)

		return short_summary, long_summary

	def export_summary_to_file(self, lines):
		with open(self.game.summary_path, 'w') as file:
			file.writelines(lines)

	def draw_pitch_control(self, pitch_control):
		self.viewer.pause_animation()
		self.viewer.reset_pitch()
		self.ui.canvasLayout.removeWidget(self.canvas)
		self.viewer.reset_pitch()

		# away_colors, home_colors = self.prepare_gradients(events)
		show_text = self.ui.showEventTextOnPitchCheckBox.isChecked()
		self.viewer.draw_pitch_control(pitch_control)

		self.canvas = FigureCanvas(self.viewer.fig)
		self.add_canvas_to_canvas_layout(self.canvas)

	def pitch_control_clicked(self):
		print("Computing Pitch Control...")
		game = self.viewer.game
		current_frame = game.frames[self.viewer.i]
		# resolution_factor = 2
		pitch_control = self.viewer.game.compute_pitch_control(current_frame)
		self.draw_pitch_control(pitch_control)
		self.viewer.show_frame_by_frame_id(current_frame.id)

	def xG_clicked(self):
		print('Printing xG model...')
		game = self.viewer.game
		current_frame = game.frames[self.viewer.i]
		xG = game.compute_xG(current_frame)
		self.draw_xG(xG)

	def draw_xG(self, xG):
		self.viewer.pause_animation()
		self.viewer.reset_pitch()
		self.ui.canvasLayout.removeWidget(self.canvas)
		self.viewer.reset_pitch()

		self.viewer.draw_xG(xG)
		self.canvas = FigureCanvas(self.viewer.fig)
		self.add_canvas_to_canvas_layout(self.canvas)

	def my_index_clicked(self):
		print('Printing combination of xG & Pitch Control...')
		game = self.viewer.game
		current_frame = game.frames[self.viewer.i]
		index = game.compute_index(current_frame)

		self.draw_my_index(index)

	def draw_my_index(self, my_index):
		self.viewer.pause_animation()
		self.viewer.reset_pitch()
		self.ui.canvasLayout.removeWidget(self.canvas)

		self.viewer.draw_my_index(my_index)
		self.canvas = FigureCanvas(self.viewer.fig)
		self.add_canvas_to_canvas_layout(self.canvas)


	def draw_players_clicked(self):
		self.viewer.show_frame_by_frame_id(self.game.frames[self.viewer.i].id)
		# self.viewer.plot_players_from_frame(self.game.frames[self.viewer.i])

	def get_no_max(self):
		return self.ui.extra_spin_box.value()

	def get_square(self):
		return self.ui.extra_spin_box2.value()

class TaskThread(QtCore.QThread):
	taskFinished = QtCore.pyqtSignal()

	def __init__(self, viewer, game, xmlWriter, dir_path, game_id):
		super().__init__()
		self.viewer = viewer
		self.game = game
		self.xmlWriter = xmlWriter
		self.dir_path = dir_path
		self.game_id = game_id

	def run(self):
		generator = Aligner(self.game)
		self.game.load_xml_data(use_original_file=True)
		generator.generate_file_without_synchronization()
		generator.adjust_events()
		root = self.xmlWriter.generate_event_details_xml(self.game)
		text = self.xmlWriter.prettify(root)
		orig = "<?my_xml version=\"1.0\" ?>"
		text2 = text.replace(orig, Constants.xml_defining_string_16)
		path = self.game.event_data_improved_path
		with open(path, 'w') as file:
			file.write(text2)

		self.game.load()
		self.taskFinished.emit()
