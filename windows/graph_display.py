import csv
from collections import Counter

from windows import GraphViewer
import Launcher
from sequence.Aligner import Aligner
from typedefs.EventType import EventType
import numpy as np
import pickle

from typedefs.QuallifierType import QualifierType


def generate_data():
	analyzed_events = [EventType.PASS,
				   EventType.OFFSIDE_PASS,
				   EventType.CLEARANCE,
				   EventType.MISS,
				   EventType.POST,
				   EventType.ATTEMPT_SAVED,
				   EventType.BALL_TOUCH,
				   EventType.AERIAL,
				   EventType.TACKLE,
				   EventType.TAKE_ON,
				   EventType.CHALLENGE,
				   EventType.DISPOSSESSED,
				   EventType.BALL_RECOVERY,
				   EventType.INTERCEPTION,
				   EventType.KEEPER_PICK_UP,
				   EventType.KEEPER_SWEEPER,
				   EventType.CROSS_NOT_CLAIMED,
				   EventType.SMOTHER]

	game_numbers = [1059341, 1059342, 1059343, 1059344, 1059345, 1059346, 1059347]
	# game_numbers = [1059341, 1059343, 1059344, 1059345, 1059346]
	# game_numbers = [1059341]
	# game_numbers = [1059194]

	# game_number = 1059253
	# game = Launcher.initiate("/home/pawel/SDU/MT/mt_project/data/1059253", game_number,10000)
	# game.load()

	games = []
	events_improved = []
	events = []
	time_diff = []
	x_diff = []
	y_diff = []
	try:

		for i in game_numbers:
			print("Game {0} started...".format(i))
			game = Launcher.initiate("D:\master_data\data\many_games", i)
			# game = Launcher.initiate("D:\master_data\data\Week 1 - Season 2019-2020\SonderjyskE vs Randers", i)
			game.load()
			events.extend(game.events)

			adjust_events(game)
			events_improved.extend(game.events)

			orig_events = [event for event in game.events if
						   event.type != EventType.RECEPTION and event.type != EventType.CARRY]
			for event in orig_events:
				if not event.is_aligned():
					continue

				orig_idx = game.get_index_by_time(event.min_orig, event.sec_orig, event.period_id)
				orig_frame_id = game.frames[orig_idx].id
				sync_frame_id = event.frame_id
				time_diff.append(sync_frame_id - orig_frame_id)  # frame difference after alignment

				if event.type in analyzed_events:
					x_diff.append(event.x - event.x_orig)
					y_diff.append(event.y - event.y_orig)

			print("Game {0} ended...".format(i))
			print("Original Events: {0}".format(len(orig_events)))
			print("Original Frames: {0}".format(len(game.frames)))
	except:
		pass
		# with open('results/time_diff_47.csv', 'w', newline='') as myfile:
		# 	wr = csv.writer(myfile)
		# 	wr.writerow(time_diff)
		# with open('results/x_diff_47.csv', 'w', newline='') as myfile:
		# 	wr = csv.writer(myfile)
		# 	wr.writerow(x_diff)
		# with open('results/y_diff_47.csv', 'w', newline='') as myfile:
		# 	wr = csv.writer(myfile)
		# 	wr.writerow(y_diff)

	with open('../results/time_diff_good_games_2.csv', 'w', newline='') as myfile:
		wr = csv.writer(myfile)
		wr.writerow(time_diff)
	with open('../results/x_diff_good_games_2.csv', 'w', newline='') as myfile:
		wr = csv.writer(myfile)
		wr.writerow(x_diff)
	with open('../results/y_diff_good_games_2.csv', 'w', newline='') as myfile:
		wr = csv.writer(myfile)
		wr.writerow(y_diff)

	stats = calculate_event_stats(events)
	# with open('results/stats_1059194_ma3.pkl', 'wb') as f:
	# 	pickle.dump(stats, f, pickle.HIGHEST_PROTOCOL)

	GraphViewer.show_histogram(stats)

	stats = calculate_event_stats(events_improved)
	GraphViewer.show_histogram(stats)


def read_data(suffix=""):
	time_diff = []
	with open('results/time_diff{0}.csv'.format(suffix), 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			time_diff.append(float(value))
	stats = prepare_bars(time_diff)
	neg_time_diff = [-x for x in time_diff]

	print("time std: {0}".format(np.std(neg_time_diff)))
	print("time var: {0}".format(np.var(neg_time_diff)))
	print("time avg: {0}".format(np.average(neg_time_diff)))
	print("time mean: {0}".format(np.mean(neg_time_diff)))


	dist_diff = []
	x_diff = []
	y_diff = []
	with open('results/x_diff{0}.csv'.format(suffix), 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			x_diff.append(float(value))
	with open('results/y_diff{0}.csv'.format(suffix), 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			y_diff.append(float(value))

	for i in range(len(x_diff)):
		x_meters = 105 * x_diff[i] / 100
		y_meters = 68 * y_diff[i] / 100
		dist = x_meters ** 2 + y_meters ** 2
		dist_diff.append(np.sqrt(dist))

	print("dist std first 100: {0}".format(np.std(dist_diff[:100])))
	print("dist std: {0}".format(np.std(dist_diff)))
	print("dist var: {0}".format(np.var(dist_diff)))
	print("dist avg: {0}".format(np.average(dist_diff)))
	print("dist mean: {0}".format(np.mean(dist_diff)))

	GraphViewer.show_time_diff_distribution(neg_time_diff)
	GraphViewer.show_dist_diff_distribution(dist_diff)
	GraphViewer.show_distance_error(x_diff, y_diff)


def prepare_bars(values):
	ret_dict = []
	for val in values:
		ret_dict.append(val)
	counts = Counter(ret_dict)
	number_of_events = len(values)
	# for k in counts:
	# 	counts[k] = counts[k] / number_of_events * 100
	return counts


def show_f24_ma3_comparison():
	stats1 = None
	stats2 = None
	with open('../results/stats_1059194_f24.pkl', 'rb') as input:
		stats1 = pickle.load(input)
	# GraphViewer.show_histogram(stats1)
	with open('../results/stats_1059194_ma3.pkl', 'rb') as input:
		stats2 = pickle.load(input)
	# GraphViewer.show_histogram(stats2)

	GraphViewer.show_histogram(stats=stats1, stats2=stats2)


def find_similar_passes():
	game_numbers = [1059334, 1059361]

	for i in range(1059334,1059361):
		if i == 1059338:
			continue

		print("Game {0} started...".format(i))
		game = Launcher.initiate("D:\master_data\data\many_games", i)
		# game = Launcher.initiate("D:\master_data\data\Week 1 - Season 2019-2020\SonderjyskE vs Randers", i)
		game.load_xml_data()

		x = [event for event in game.events if event.type == EventType.PASS and 46 <int(event.x_orig) <56 and 18 <int(event.y_orig) <28 and 61 < float(event.get_qualifier_value(QualifierType.PASS_END_X)) < 69 and 41 < float(event.get_qualifier_value(QualifierType.PASS_END_Y)) < 49]
		print("Game {0}: list length - {1}".format(i, len(x)))
		for event in x:
			print(event.id)




def main():
	# show_f24_ma3_comparison()


	# generate_data()
	# read_data("_1059194_F24")
	# read_data_F24_MA3()
	# read_data("_good_games")
	# read_data("_45")
	# read_polar_data()
	find_similar_passes()


def adjust_events(game):
	generator = Aligner(game)

	generator.adjust_events()
	# game.load()
	print_stats(generator.stats)


def calculate_event_stats(events):
	ret_dict = []
	for event in events:
		ret_dict.append(event.type.name)
	counts = Counter(ret_dict)
	return counts


# uncomment to get percentages
# number_of_events = len(events)
# for k in counts:
# 	counts[k] = counts[k] / number_of_events * 100
# return counts


def read_data_F24_MA3():
	time_diff_F24 = []
	with open('../results/time_diff_1059194_F24.csv', 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			time_diff_F24.append(float(value))
	stats = prepare_bars(time_diff_F24)
	neg_time_diff_F24 = [-x for x in time_diff_F24]


	time_diff_MA3 = []
	with open('../results/time_diff_1059194_MA3.csv', 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			time_diff_MA3.append(float(value))
	stats = prepare_bars(time_diff_MA3)
	neg_time_diff_MA3 = [-x for x in time_diff_MA3]



	dist_diff_MA3 = []
	dist_diff_F24 = []
	x_diff_MA3 = []
	x_diff_F24 = []
	y_diff_MA3 = []
	y_diff_F24 = []
	with open('../results/x_diff_1059194_MA3.csv', 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			x_diff_MA3.append(float(value))
	with open('../results/x_diff_1059194_F24.csv', 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			x_diff_F24.append(float(value))



	with open('../results/y_diff_1059194_MA3.csv', 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			y_diff_MA3.append(float(value))
	with open('../results/y_diff_1059194_F24.csv', 'r') as csvfile:
		mylist = [row for row in csv.reader(csvfile, delimiter=',')][0]
		for value in mylist:
			y_diff_F24.append(float(value))

	for i in range(len(x_diff_MA3)):
		x_meters = 105 * x_diff_MA3[i] / 100
		y_meters = 68 * y_diff_MA3[i] / 100
		dist = x_meters ** 2 + y_meters ** 2
		dist_diff_MA3.append(np.sqrt(dist))
	for i in range(len(x_diff_F24)):
		x_meters = 105 * x_diff_F24[i] / 100
		y_meters = 68 * y_diff_F24[i] / 100
		dist = x_meters ** 2 + y_meters ** 2
		dist_diff_F24.append(np.sqrt(dist))

	ma3_avg = np.average(dist_diff_MA3)
	f24_avg = np.average(dist_diff_F24)
	print("MA3 avg: ", ma3_avg)
	print("F24 avg: ", f24_avg)

	GraphViewer.show_time_diff_distribution_F24_MA3(neg_time_diff_F24, neg_time_diff_MA3)
	GraphViewer.show_dist_diff_distribution_F24_MA3(dist_diff_F24, dist_diff_MA3)
	GraphViewer.show_distance_error_F24_MA3(x_diff_F24, y_diff_F24, x_diff_MA3, y_diff_MA3)


def print_stats(stats):
	print("Pass variance: {0:.2f}".format(stats.pass_variance()))
	print("Pass std dev: {0:.2f} frames".format(stats.pass_std_dev()))
	print("Pass avg: {0:.2f} frames".format(stats.pass_avg()))
	print("State variance: {0:.2f}".format(stats.state_variance()))
	print("State std dev: {0:.2f} frames".format(stats.state_std_dev()))
	print("State avg: {0:.2f} frames".format(stats.state_avg()))
	print("Mismatched events: {0}".format(stats.mismatched_events))


if __name__ == '__main__':
	main()
