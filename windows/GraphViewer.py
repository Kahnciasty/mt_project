import operator

import matplotlib.pyplot as plt
import numpy as np

import MathHelper


def show_histogram(stats, show_values=False, stats2=None):
	sorted_stat = sorted(stats.items(), key=operator.itemgetter(1))
	keys = []
	values = []
	values2 = []
	for k, v in reversed(sorted_stat):
		keys.append(k)
		values.append(v)
		if stats2 is not None:
			values2.append(stats2[k])

	y_pos = np.arange(len(keys))
	fig, ax = plt.subplots()
	plt.grid(True)
	plt.rc('axes', titlesize=20)
	plt.rc('font', size=15)
	ax.set_axisbelow(True)
	ax.set_title("Events distribution")
	# ax.set_xlabel("Events")
	ax.set_ylabel("# of events", fontsize=20)

	w = 0.4
	stats1_bars = ax.bar(y_pos -w/2, values, width=w, color='blue', align='center', label="F24 files")
	plt.legend(handles=[stats1_bars])
	plt.xticks(y_pos, keys, rotation=0, ha="center")
	if stats2 is not None:
		stats2_bars = ax.bar(y_pos+w -w/2, values2, width=w, color='red', align='center', label="MA3 files")
		plt.legend(handles=[stats1_bars, stats2_bars])

	if show_values:
		for i, v in enumerate(values):
			ax.text(i-w+.5 , v + 0.2 , "{0}".format(v), rotation=0, ha="center")
	# plt.tight_layout()
	# plt
	plt.show()

def show_time_diff_distribution(stats):
	bins = []
	for i in range(-50, 100):
		bins.append(i)

	_, _, patches = plt.hist(stats, bins, histtype='bar', rwidth=1, color='black')

	gradient = MathHelper.linear_gradient("#0000ff", "#ff0000", 100)['hex']
	for i, patch in enumerate(patches):
		color = gradient[i] if i < 100 else gradient[-1]
		patch.set_color(color)

	# factor = 30
	# blue_green_gradient = MathHelper.linear_gradient("#0000ff", "#00ff00", factor)['hex']
	# green_red_gradient = MathHelper.linear_gradient("#00ff00", "#ff0000", factor)['hex']
	#
	# for i, patch in enumerate(patches):
	# 	if i < 20:
	# 		color = blue_green_gradient[0]
	# 	elif i <50:
	# 		color = blue_green_gradient[i-20]
	# 	elif i < 80:
	# 		print(i)
	# 		color = green_red_gradient[i-factor - 20]
	# 	else:
	# 		color = green_red_gradient[-1]
	#
	#
	# 	# color = gradient[i] if i < 99 else gradient[-1]
	# 	patch.set_color(color)
	# 	if patch.xy[0] == 0:
	# 		patch.set_height(180)
	# 	# if patch.xy[0] < 0:
	# 	# 	patch.set_color('b')
	# 	# elif patch.xy[0] >0:
	# 	# 	patch.set_color('r')

	plt.rc('axes', titlesize=20)
	plt.rc('font', size=33)
	plt.xlabel('Difference [frames]', fontsize=20)
	plt.xticks(fontsize=15)
	plt.yticks(fontsize=15)
	plt.ylabel('# of occurrences', fontsize=20)
	plt.grid(True)
	plt.title('A distribution of time difference of events before and after the synchronization')

	plt.legend()
	plt.show()

def show_dist_diff_distribution(stats):
	bins = []
	for i in range(0, 25):
		bins.append(i)

	bins = np.arange(0, 25, 0.4)
	stats = stats[:11544]

	_, _, patches = plt.hist(stats, bins, histtype='bar', rwidth=1, color='black')
	gradient = MathHelper.linear_gradient("#00ff00", "#ff0000", 20)['hex']
	for i, patch in enumerate(patches):
		color = gradient[i] if i < 20 else gradient[-1]
		patch.set_color(color)
		# if patch.xy[0] < 0.05:
		# 	patch.set_height(17)

	plt.rc('axes', titlesize=20)
	plt.rc('font', size=33)
	plt.xlabel('Difference [m]', fontsize=20)
	plt.ylabel('# of occurrences', fontsize=20)
	plt.xticks(fontsize=15)
	plt.yticks(fontsize=15)
	plt.grid(True)
	plt.title('A distribution of distance difference of events before and after the synchronization')
	# plt.title('A distribution of distance difference of events in MA3 files')

	plt.xlim(-1, 20)
	# plt.legend()
	plt.show()

	zero_five = [x for x in stats if x <5]
	five_ten = [x for x in stats if 5<=x<10]
	ten_fifteen = [x for x in stats if 10<=x<15]
	fifteen_twenty = [x for x in stats if 15<=x<20]
	mote_than_twenty = [x for x in stats if x>20]

	print(len(zero_five))
	print(len(five_ten))
	print(len(ten_fifteen))
	print(len(fifteen_twenty))
	print(len(mote_than_twenty))



def show_dist_diff_distribution_F24_MA3(stats_F24, stats_MA3):
	bins = []
	for i in range(0, 25):
		bins.append(i)

	bins = np.arange(0, 25, 0.4)

	_, _, patches = plt.hist(stats_F24, bins, histtype='bar', rwidth=1, color='black',label="F24 files")
	for i, patch in enumerate(patches):
		color = "#FF000050"
		patch.set_color(color)
		if patch.xy[0] < 0.05:
			patch.set_height(17)

	_, _, patches = plt.hist(stats_MA3, bins, histtype='bar', rwidth=1, color='black',label="MA3 files")
	for i, patch in enumerate(patches):
		color = "#0000FF50"
		patch.set_color(color)
		if patch.xy[0] < 0.05:
			patch.set_height(25)

	plt.rc('axes', titlesize=20)
	plt.rc('font', size=20)
	plt.xlabel('Difference [m]', fontsize=20)
	plt.ylabel('# of occurrences', fontsize=20)
	plt.xticks(fontsize=15)
	plt.yticks(fontsize=15)
	plt.grid(True)
	# plt.title('A distribution of distance difference of events before and after the synchronization')
	plt.title('A comparison of distribution of distance difference of events in MA3 and F24 files')

	plt.xlim(-1, 20)
	plt.legend()
	plt.show()


def show_time_diff_distribution_F24_MA3(stats_F24, stats_MA3):
	bins = []
	for i in range(-50, 100):
		bins.append(i)

	_, _, patches_f24 = plt.hist(stats_F24, bins, histtype='bar', rwidth=1, color="#FF000050",label="F24 files")
	_, _, patches_ma3 = plt.hist(stats_MA3, bins, histtype='bar', rwidth=1, color="#0000FF50",label="MA3 files")

	for patch in patches_f24:
		if patch._height > 40:
			patch.set_height(13)

	for patch in patches_ma3:
		if patch._height > 40:
			patch.set_height(17)


	plt.rc('axes', titlesize=20)
	plt.rc('font', size=20)
	plt.xlabel('Difference [frames]', fontsize=20)
	plt.xticks(fontsize=15)
	plt.yticks(fontsize=15)
	plt.ylabel('# of occurrences', fontsize=20)
	plt.grid(True)
	plt.title('A distribution of time difference of events before and after the synchronization')

	plt.legend()
	plt.show()


def show_distance_error(x, y):
	plt.scatter(x, y, c="grey", s=5)


	for r in range(5,100,5):
		theta = np.linspace(0, 2 * np.pi, 100)
		x1 = r * np.cos(theta)
		x2 = r * np.sin(theta)
		plt.plot(x1, x2, c="black", linewidth=1, linestyle=(0, (7, 10)))
		plt.text(-1, -r, "{0} meters".format(r), bbox=dict(facecolor='white', alpha=0.8))
	theta = np.linspace(0, 2 * np.pi, 100)
	x1 = 1 * np.cos(theta)
	x2 = 1 * np.sin(theta)
	plt.plot(x1, x2, c="black", linewidth=1, linestyle=(0, (7, 10)))
	# plt.text(-1, -1, "{0} meters".format(1), bbox=dict(facecolor='white', alpha=0.8))

	plt.xlim(-20,20)
	plt.ylim(-20,20)
	plt.xlabel('[metres]', fontsize=20)
	plt.ylabel('[metres]', fontsize=20)
	# plt.legend()
	plt.show()



def show_distance_error_F24_MA3(x_diff_F24, y_diff_F24, x_diff_MA3, y_diff_MA3):
	# plt.scatter(x_diff_MA3, y_diff_MA3, c="#0000FF50", s=15, label="MA3 files")
	# plt.title('A difference in location for events provided in MA3 files')
	plt.scatter(x_diff_F24, y_diff_F24, c="#FF000050", s=15,label="F24 files")
	# plt.title('A difference in location for events provided in F24 files')


	for r in range(5,21,5):
		theta = np.linspace(0, 2 * np.pi, 100)
		x1 = r * np.cos(theta)
		x2 = r * np.sin(theta)
		plt.plot(x1, x2, c="black", linewidth=1, linestyle=(0, (7, 10)))
		plt.text(-1, -r, "{0} meters".format(r), bbox=dict(facecolor='white', alpha=0.8))
	theta = np.linspace(0, 2 * np.pi, 100)
	x1 = 1 * np.cos(theta)
	x2 = 1 * np.sin(theta)
	plt.plot(x1, x2, c="black", linewidth=1, linestyle=(0, (7, 10)))
	# plt.text(-1, -1, "{0} meters".format(1), bbox=dict(facecolor='white', alpha=0.8))

	plt.xlim(-20,20)
	plt.ylim(-20,20)
	plt.xlabel('[metres]', fontsize=20)
	plt.ylabel('[metres]', fontsize=20)
	plt.xticks(fontsize=15)
	plt.yticks(fontsize=15)

	plt.show()