

class FrameEvent:
	def __init__(self, id, mismatched, frame_id):
		self.id = id
		self.mismatched = mismatched
		self.frame_id = frame_id



def from_line(event_as_line, frame_id) -> "FrameEvent":
	items = event_as_line.split(",")
	id = items[0]
	mismatched = False
	if items[1] == "T":
		mismatched = True
	return FrameEvent(id, mismatched, frame_id)