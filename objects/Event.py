import random

from objects.Coord import Coord
from typedefs.EventType import EventType
from objects.Qualifier import Qualifier
from typedefs.QuallifierType import QualifierType
from my_xml.XmlAttributes import XmlAttributes


class Event:
	def __init__(self, id=None, event_id=-1, type_id=-1, period_id=-1, min=-1, sec=-1, team_id="",
				 outcome=None, x=-1, y=-1, timestamp=None, last_modified=None,
				 version=None, player_id=None):
		self.id = id
		self.event_id = int(event_id)
		self.type = EventType(int(type_id))
		self.period_id = int(period_id)
		self.min = int(min)
		self.sec = int(sec)
		self.min_orig = int(min)
		self.sec_orig = int(sec)
		self.team_id = team_id
		self.outcome = outcome
		# ↓ coord of event on the pitch [m]
		self.coord = Coord(float(x), float(y))
		self.coord_end = None
		# ↓ original coords from event data (0-100)
		self.x = float(x)
		self.y = float(y)
		self.x_orig = float(x)
		self.y_orig = float(y)
		self.timestamp = timestamp
		self.last_modified = last_modified
		self.version = version
		self.mismatched = False

		if player_id is None or player_id == "None":
			self.player_id = None
		else:
			self.player_id = player_id
		self.qualifiers = []
		self.frame_id = None
		self.player = None
		self.team = None
		self.reception_id = None
		self.player_corrected = False

	# comment this ↓ method, if you want to show event details in debug mode. Uncomment if you want to export the events to xml file
	def __dict__(self):
		ret = dict()
		ret[XmlAttributes.ID] = str(self.id)
		ret[XmlAttributes.EVENT_ID] = str(self.event_id)
		ret[XmlAttributes.TYPE_ID] = str(self.type.value)
		ret[XmlAttributes.PERIOD_ID] = str(self.period_id)
		ret[XmlAttributes.MIN] = str(self.min)
		ret[XmlAttributes.SEC] = str(self.sec)
		ret[XmlAttributes.TEAM_ID] = str(self.team_id)
		ret[XmlAttributes.OUTCOME] = str(self.outcome)
		ret[XmlAttributes.X] = str(self.x)
		ret[XmlAttributes.Y] = str(self.y)
		ret[XmlAttributes.TIMESTAMP] = str(self.timestamp)
		ret[XmlAttributes.LAST_MODIFIED] = str(self.last_modified)
		ret[XmlAttributes.VERSION] = str(self.version)
		if self.mismatched:
			ret[XmlAttributes.MISMATCHED] = str(self.mismatched)
		if self.frame_id is not None:
			ret[XmlAttributes.FRAME_ID] = str(self.frame_id)
		ret[XmlAttributes.PLAYER_ID] = str(self.player_id)
		if self.reception_id is not None:
			ret[XmlAttributes.RECEPTION_ID] = str(self.reception_id)
		if self.player_corrected:
			ret[XmlAttributes.PLAYER_CORRECTED] = str(self.player_corrected)
		return ret

	def is_aligned(self):
		return self.frame_id is not None

	def __str__(self):
		mis = "MISMATCHED" if self.mismatched else ""
		return "id: {3}; {0}; player: {1}; {2}".format(self.type.name, str(self.player), mis, self.id)

	def has_end_coords(self):
		coords = [q for q in self.qualifiers if
				  q.type == QualifierType.PASS_END_X or q.type == QualifierType.PASS_END_Y]
		return len(coords) != 0

	def get_qualifier_value(self, q_type):
		for q in self.qualifiers:
			if q.qualifier_id == q_type.value:
				return q.value
		return None

	def get_qualifier(self, q_type):
		for q in self.qualifiers:
			if q.qualifier_id == q_type.value:
				return q
		return None

	def ends_in_penalty_box(self):
		x = self.x
		y = self.y

		x_end = float(self.get_qualifier_value(QualifierType.PASS_END_X))
		y_end = float(self.get_qualifier_value(QualifierType.PASS_END_Y))
		starts_outside = x < 85 or y < 20 or y > 80
		ends_inside = x_end > 85 and 20 < y_end < 80
		return starts_outside and ends_inside

	def on_left_side(self):
		return self.y > 80

	def on_right_side(self):
		return self.y < 20

	def on_center_side(self):
		return not self.on_left_side() and not self.on_right_side()

	def on_back_side(self):
		return self.x < 50

	def on_front_side(self):
		return not self.on_back_side()

	def display_order(self, freq, first_frame_id):
		if self.frame_id is not None:
			return self.frame_id
		frames_since_start = freq * ((self.period_id - 1) * 60 * 45 + self.min * 60 + self.sec)
		return first_frame_id + frames_since_start

	def is_successful(self):
		return self.outcome == "1"

	def determine_attack_direction(self):
		if self.period_id == 1:
			return self.player.team.first_half_direction
		else:
			return self.player.team.second_half_direction

	def get_packing_rate(self):
		return self.get_qualifier_value(QualifierType.PACKING_RATE)

	def get_closest_opponent_distance(self):
		return self.get_qualifier_value(QualifierType.CLOSEST_OPPONENT_DISTANCE)

	def get_passing_sequence(self):
		return self.get_qualifier_value(QualifierType.PASS_SEQUENCE)

	def add_qualifier(self, q_type, value=None):
		for qualifier in self.qualifiers:
			if qualifier.type == q_type:
				self.qualifiers.remove(qualifier)
				continue

		q_type_id = q_type.value
		value_str = str(value) if value is not None else None
		id_str = self.create_random_id()
		q = Qualifier(id_str, q_type_id, value_str)
		self.qualifiers.append(q)

	def create_random_id(self):
		return str(random.randint(0, 10000000))

	# Order reception events before carry events (they always happen at exact same time, but reception is always first)
	def same_minute_order(self):
		if self.type == EventType.RECEPTION:
			return 0
		elif self.type == EventType.CARRY:
			return 1

		return 2
