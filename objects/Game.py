import datetime
from timeit import default_timer as timer
import os
import time
from bisect import bisect_left
from collections import Counter

import numpy as np
import statsmodels.api as sm

from objects import Frame
import MathHelper
from typedefs.AttackDirection import AttackDirection
from objects.Coord import Coord
from typedefs.EventType import EventType
from players.PassingNetworkPlayer import PassingNetworkPlayer
from typedefs.QuallifierType import QualifierType
from typedefs.TeamType import TeamType
from my_xml import XmlHelper
from my_xml.XmlAttributes import XmlAttributes
from my_xml.XmlTags import XmlTags


class Game:
	def __init__(self, location_data, event_data, match_data, meta_data, synced_location_data_path,
				 asynced_location_data_path, event_data_improved, summary_path, lines, loader):
		self.frames = []
		self.frame_ids = []
		self.match_data_path = match_data
		self.event_data_path = event_data
		self.event_data_improved_path = event_data_improved
		self.meta_data_path = meta_data
		self.location_data_path = location_data
		self.synced_location_data_path = synced_location_data_path
		self.asynced_location_data_path = asynced_location_data_path
		self.summary_path = summary_path
		_, self.filename = os.path.split(location_data)
		self.number_of_lines_to_load = lines
		self.first_period_frame_id = 0
		self.events = []
		# self.mismatched_events = []
		self.players = []
		self.teams = []
		self.game_summary = None
		self.position_frequency = 25  # [Hz]
		self.period_metadata = None
		self.match_metadata = None
		# self.loader = MA3XmlLoader()
		self.loader = loader
		self.aligning_stats = None

		self.pc_resolution_factor = 1 # 1 is to speed up the computations. Change back to 2

	def load(self):
		self.load_xml_data()
		self.load_location_data()
		self.events.sort(key=lambda event: (event.period_id, event.min, event.sec, event.same_minute_order()))

	def load_xml_data(self, use_original_file=False):
		self.loader.load(self, use_original_file)
		self.period_metadata = self.load_metadata_period()
		self.match_metadata = self.load_metadata_match()
		self.first_period_frame_id = self.get_frame_id_By_Xml_Attribute(XmlAttributes.ISTARTFRAME, 1)
		self.first_period_end_frame_id = self.get_frame_id_By_Xml_Attribute(XmlAttributes.IENDFRAME, 1)
		self.second_period_frame_id = self.get_frame_id_By_Xml_Attribute(XmlAttributes.ISTARTFRAME, 2)
		self.second_period_end_frame_id = self.get_frame_id_By_Xml_Attribute(XmlAttributes.IENDFRAME, 2)
		self.position_frequency = int(self.match_metadata[0].attributes[XmlAttributes.FREQUENCY].value)
		self.improve_events()

	def load_location_data(self, use_async=False):
		start = time.time()
		lines = 0
		self.frames = []

		if use_async:
			path = self.asynced_location_data_path
		else:
			path = self.synced_location_data_path

		used_location_file = path if os.path.exists(path) else self.location_data_path
		for line in open(used_location_file, 'r'):
			line = line.rstrip()

			lines += 1
			frame = Frame.from_line(line)
			if len(self.frames) > 1:
				frame.ball.speed, frame.ball.vector = MathHelper.calculate_ball_movement(frame, self.frames[-1])

			self.frames.append(frame)
			for frame_event in frame.frame_events:
				event = self.get_event_by_event_id(frame_event.id)
				if event is None:
					continue

				event.frame_id = frame_event.frame_id
				if frame_event.mismatched:
					event.mismatched = True

			if lines == self.number_of_lines_to_load:  # Load only the beginning of the game
				break
		self.frame_ids = [f.id for f in self.frames]

		end = time.time()
		print(
			"The game contains {0} lines which corresponds to {1:.2f} minutes of the game.".format(len(self.frames),
																								   len(self.frames) / (
																									   self.position_frequency) / 60))
		elapsed_time = end - start
		print("Loading location took {0:.2f} seconds".format(elapsed_time))

	def load_metadata_period(self):
		doc = XmlHelper.load_xml(self.meta_data_path)
		return doc.getElementsByTagName(XmlTags.PERIOD)

	def load_metadata_match(self):
		doc = XmlHelper.load_xml(self.meta_data_path)
		return doc.getElementsByTagName(XmlTags.MATCH)

	def load_game_summary(self):
		summary = self.loader.load_game_summary(self)
		return summary

	def determine_event_text(self, event):
		mismatched_text = " Mismatched" if event.mismatched and not event.player_corrected else ""
		player = event.player
		if player is None:
			return str(event.type)
		else:
			team = self.get_team_by_team_type(player.team_type)
			player_corrected_text = ""
			if event.player_corrected:
				player_corrected_text = "(Player Corrected)"

			return '{0}:{1:02d} {2} -{3} {4} {5} {6} {7}'.format(event.min, event.sec, event.type,
																 mismatched_text, player.last_name,
																 player.number,
																 team.name, player_corrected_text)

	def determine_short_event_text(self, event):
		for player in self.players:
			if event.player_id is None:
				return str(event.type)
			else:
				if event.player_id == player.player_ref:
					return '{0} {1} {2}'.format(event.type, player.last_name, player.number, )
		return ""

	def get_frame_id_By_Xml_Attribute(self, xml_attribute, part):
		frame_id = None
		for period in self.period_metadata:
			if int(period.attributes[XmlAttributes.IID].value) == part:
				frame_id = int(period.attributes[xml_attribute].value)
				break
		return frame_id

	def get_index_by_id(self, id):
		if id < self.frames[0].id:
			return 0

		i = bisect_left(self.frame_ids, id)
		if i != len(self.frame_ids) and self.frame_ids[i] == id:
			return i
		raise ValueError

	def get_tracking_player_from_event(self, event, frame):
		if event.player_id is None:
			return None

		player = event.player
		return self.get_tracking_player_from_event_player(player, frame)

	def get_tracking_player_from_event_player(self, event_player, frame):
		if event_player is None:
			return None
		return next(
			(p for p in frame.players if p.number == event_player.number and p.team_type == event_player.team_type),
			None)

	def get_event_player_from_event(self, event):
		if event.player_id is None:
			return None

		for player in self.players:
			if event.player_id == player.player_ref:
				return player
		return None

	def get_event_player_by_tracking_player(self, track_player):
		for event_player in self.players:
			try:
				if event_player.number == track_player.number and event_player.team_type == track_player.team_type:
					return event_player
			except AttributeError:
				print("s")
		return None

	def get_pitch_dimensions(self):
		width = float(self.match_metadata[0].attributes[XmlAttributes.PITCH_WIDTH_METERS].value)
		height = float(self.match_metadata[0].attributes[XmlAttributes.PITCH_HEIGHT_METERS].value)
		return width, height

	def get_tracking_player_closest_to_ball(self, index):
		min_dist = 100000000
		closest_player = None
		try:
			correct_players = list(filter(lambda p: p.number != -1, self.frames[index].players))
		except IndexError:
			print("error1")
		for player in correct_players:
			dist = MathHelper.dist_between(player.coord, self.frames[index].ball.coord)
			if dist < min_dist:
				closest_player = player
				min_dist = dist

		if closest_player is None:
			print("error2")
		return closest_player

	def get_tracking_player_by_frame_index_and_track_player(self, index, track_player):
		if track_player is None:
			return track_player
		for player in self.frames[index].players:
			if player.number == track_player.number and player.team_type == track_player.team_type:
				return player
		return None

	def get_tracking_player_by_frame_and_track_player(self, frame, track_player):
		if track_player is None:
			return track_player
		for player in frame.players:
			if player.number == track_player.number and player.team_type == track_player.team_type:
				return player
		return None

	def improve_events(self):
		for event in self.events:
			if event.player_id is not None:
				event.player = self.get_event_player_from_event(event)

			if event.team_id is not None:
				event.team = self.get_team_by_id(event.team_id)

			self.set_events_coords(event)

	def set_events_coords(self, event):
		factor = 1
		# in first period away team is in right half
		if event.team_id == self.get_team_by_team_type(TeamType.AWAY).id and event.period_id == 1:
			factor = -1
		# in second period home team is in the right half
		if event.team_id == self.get_team_by_team_type(TeamType.HOME).id and event.period_id == 2:
			factor = -1

		# set track position of an event from home team perspective in the first half (on left side)
		width, height = self.get_pitch_dimensions()
		x = event.x
		y = event.y
		x = width * (x / 100)
		y = height * (y / 100)
		x = x - width / 2
		y = y - height / 2
		x *= 100
		y *= 100
		x *= factor
		y *= factor
		event.coord.x = x
		event.coord.y = y

		if event.has_end_coords():
			x = float(event.get_qualifier_value(QualifierType.PASS_END_X))
			y = float(event.get_qualifier_value(QualifierType.PASS_END_Y))
			x = width * (x / 100)
			y = height * (y / 100)
			x = x - width / 2
			y = y - height / 2
			x *= 100
			y *= 100
			x *= factor
			y *= factor
			event.coord_end = Coord(x, y)

	def set_event_position_by_track_player(self, event, track_player):
		width, height = self.get_pitch_dimensions()

		x = track_player.coord.x
		y = track_player.coord.y
		x = x + width * 100 / 2
		y = y + height * 100 / 2
		x = x / width
		y = y / height
		# in first period away team is in right half
		if event.team_id == self.get_team_by_team_type(TeamType.AWAY).id and event.period_id == 1:
			x = 100 - x
			y = 100 - y
		# in second period home team is in the right half
		if event.team_id == self.get_team_by_team_type(TeamType.HOME).id and event.period_id == 2:
			x = 100 - x
			y = 100 - y

		event.x = x
		event.y = y

	def get_index_by_time(self, minute, second, period):
		sec_since_period_start = minute * 60 + second - (period - 1) * 45 * 60
		if period == 1:
			return self.get_index_by_id(
				self.first_period_frame_id) + sec_since_period_start * self.position_frequency
		else:
			return self.get_index_by_id(
				self.second_period_frame_id) + sec_since_period_start * self.position_frequency

	def get_surrounding_aligned_events(self, event):
		idx = self.events.index(event)
		after_event = None
		before_event = None
		for i in range(idx + 1, len(self.events)):
			if self.events[i].is_aligned():
				after_event = self.events[i]
				break
		for i in range(idx - 1, 0, -1):
			if self.events[i].is_aligned():
				before_event = self.events[i]
				break

		return before_event, after_event

	def get_pass_reception(self, event):
		idx = self.events.index(event)
		for i in range(idx + 1, len(self.events)):
			if self.events[i].type == EventType.RECEPTION and self.events[i].is_aligned():
				return self.events[i]
		return None

	def get_event_by_event_id(self, id):
		for event in self.events:
			if event.id == id:
				return event
		return None

	def get_mismatched_events(self):
		return [event for event in self.events if event.mismatched]

	def get_players_by_team_type_from_frame(self, team, frame):
		return [player for player in frame.players if player.team_type == team]

	# if diff is positive, the event according to alignment happened later than in raw event data
	def get_aligned_event_time_difference(self, event):
		if not event.is_aligned():
			return 0

		if event.period_id == 1:
			frames_from_start_to_event = event.frame_id - self.first_period_frame_id
			event_orig_time_in_frames = self.position_frequency * (event.min * 60 + event.sec)
			diff = frames_from_start_to_event - event_orig_time_in_frames
			return diff

		elif event.period_id == 2:
			frames_from_period_start_to_event = event.frame_id - self.second_period_frame_id
			event_orig_time_in_frames = self.position_frequency * ((event.min - 45) * 60 + event.sec)
			diff = frames_from_period_start_to_event - event_orig_time_in_frames
			return diff

	def get_team_by_id(self, id):
		for team in self.teams:
			if team.id == id:
				return team
		return None

	def get_team_by_team_type(self, team_type):
		for team in self.teams:
			if team.team_type == team_type:
				return team
		return None

	def get_goals_by_team_type_and_frame(self, team_type, frame):
		# find goal events before given frame
		goal_events = [event for event in self.events if event.type == EventType.GOAL]
		team = self.get_team_by_team_type(team_type)
		goals = 0
		for event in goal_events:
			if not event.is_aligned():
				continue
			if event.frame_id < frame.id and event.team_id == team.id:
				goals += 1
		return goals

	def get_event_index_by_event(self, event):
		self.events.index(event)

	def get_player_by_id(self, ref):
		for player in self.players:
			if player.player_ref == ref:
				return player
		return None

	def get_team_by_name(self, name):
		for team in self.teams:
			if team.name == name:
				return team
		return None

	def get_period_id_of_frame(self, frame):
		if frame.id < self.second_period_frame_id:
			return 1
		return 2

	def contains_reception_events(self):
		for event in self.events:
			if event.type == EventType.RECEPTION:
				return True
		return False

	def get_aligned_events(self):
		return [event for event in self.events if event.is_aligned()]

	def get_frame_by_id(self, id):
		idx = self.get_index_by_id(id)
		return self.frames[idx]

	def determine_attack_direction(self, team_id, period_id):
		period_start_event = [event for event in self.events if
							  event.type == EventType.START and event.period_id == period_id and event.team_id == team_id]
		text = period_start_event[0].get_qualifier_value(QualifierType.DIRECTION_OF_PLAY)
		if text == "Left to Right":
			return AttackDirection.RIGHT
		else:
			return AttackDirection.LEFT

	def compute_passing_network(self, teamtype):
		team = self.get_team_by_team_type(teamtype)
		# consider only passes and receptions of players from the same team
		events = [event for event in self.events if event.team == team and event.frame_id is not None and (
				event.type == EventType.PASS or event.type == EventType.RECEPTION)]
		events.sort(key=lambda x: x.frame_id)
		dictionary = dict()
		for i in range(len(events) - 1):
			# eliminate from the network players that were not in starting 11
			if not events[i].player.is_starting_11():
				continue

			if events[i].type == EventType.PASS and events[i].is_successful():
				# then next event has to be a reception
				reception = events[i + 1]
				if not reception.type == EventType.RECEPTION or not reception.player.is_starting_11():
					continue

				coords = dictionary.get(events[i].player)
				if coords is None:
					dictionary[events[i].player] = []

				x = events[i].coord.x if events[i].period_id == 1 else -events[i].coord.x
				y = events[i].coord.y if events[i].period_id == 1 else -events[i].coord.y
				dictionary.get(events[i].player).append((x, y, reception.player))

		network_players = []
		for player in dictionary:
			infos = dictionary.get(player)
			x_s = []
			y_s = []
			receivers = []
			for info in infos:
				x_s.append(info[0])
				y_s.append(info[1])
				receivers.append(info[2])
			x_avg = np.average(x_s)
			y_avg = np.average(y_s)
			receivers = Counter(receivers)
			network_players.append(PassingNetworkPlayer(player, Coord(x_avg, y_avg), len(x_s), receivers))

		return network_players

	# Gives pitch control of Home (red) team from 0 to 1 where 0 is total Blue team control, and 1 is total Red team control
	# to get value of xG in position X,Y (105, 68), take pitch_control[Y*self.pc_resolution_factor, X*self.pc_resolution_factor]
	def compute_pitch_control(self, frame):
		if frame.pitch_control is not None:
			return frame.pitch_control

		#parameters
		avg_ball_speed = 15.
		int_dt = 0.04 # integration timestep (dt)
		max_int_time = 10 # max ten seconds after the ball arrives at location
		model_converge_tol = 0.01 # assume convergence when PPCF>0.99 at a given location

		start_total = time.time()

		home_players = self.get_players_by_team_type_from_frame(TeamType.HOME, frame)
		away_players = self.get_players_by_team_type_from_frame(TeamType.AWAY, frame)

		pitch_length, pitch_width = self.get_pitch_dimensions()
		pitch_length = int(pitch_length*self.pc_resolution_factor)
		pitch_width = int(pitch_width*self.pc_resolution_factor)

		intercept_times = []
		significant_players = []
		differential_times = []
		length_avg = []

		pitch_control = np.zeros((pitch_width, pitch_length))
		for i in range(0, pitch_width):
			if i % 10 == 0:
				print("PC: computing Y=", i)

			length_time = timer()
			for j in range(0, pitch_length):
				x_m = (j - pitch_length/2 + 1)/self.pc_resolution_factor  	#(0,0) is in the middle of the pitch, so we have to adjust for negative values of x and y
				y_m = - (i - pitch_width/2 + 1) /self.pc_resolution_factor  # y is the lowest at the bottom left corner, opposite to how pyplot plots. Therefore minus at the beggining
				x_cm = x_m * 100
				y_cm = y_m * 100
				intercept_coord = Coord(x_cm, y_cm)

				start = timer()
				# skip computations if difference between time of arrival is too big.
				shortest_home_time =  np.nanmin([p.compute_simple_time_to_intercept(intercept_coord, frame, self) for p in home_players])
				shortest_away_time =  np.nanmin([p.compute_simple_time_to_intercept(intercept_coord, frame, self) for p in away_players])
				end = timer()
				interval = end - start
				intercept_times.append(interval)

				ttc = home_players[0].time_to_control

				if shortest_home_time - shortest_away_time > ttc: #[sec]
					pitch_control[i][j] = 0
				elif shortest_home_time - shortest_away_time < -ttc:
					pitch_control[i][j] = 1
				else:

					start = timer()

					#Remove insignificant players
					home_players_significant = [p for p in home_players if p.time_to_intercept - shortest_home_time < p.time_to_control]
					away_players_significant = [p for p in away_players if p.time_to_intercept - shortest_away_time < p.time_to_control]

					ball_travel_time = MathHelper.dist_between_meters(intercept_coord, frame.ball.coord) / avg_ball_speed
					dT_array = np.arange(ball_travel_time - int_dt, ball_travel_time + max_int_time, int_dt)
					PPCFhome = np.zeros_like(dT_array)
					PPCFaway = np.zeros_like(dT_array)

					end = timer()
					interval = end - start
					significant_players.append(interval)

					start = timer()
					ptot = 0.0  # total pitch control probability
					k = 1
					while 1 - ptot > model_converge_tol and k < dT_array.size:
						T = dT_array[k]
						for player in home_players_significant:
							# calculate ball control probablity for 'player' in time interval T+dt
							dPPCFdT = (1 - PPCFhome[k - 1] - PPCFaway[k - 1]) * player.probability_intercept_ball(T) * player.lambda_ball_control
							player.PPCF += dPPCFdT * int_dt  # total contribution from individual player
							PPCFhome[k] += player.PPCF  # add to sum over players in the home team (remembering array element is zero at the start of each integration iteration)
						for player in away_players_significant:
							# calculate ball control probablity for 'player' in time interval T+dt
							dPPCFdT = (1 - PPCFhome[k - 1] - PPCFaway[k - 1]) * player.probability_intercept_ball(T) * player.lambda_ball_control
							player.PPCF += dPPCFdT * int_dt  # total contribution from individual player
							PPCFaway[k] += player.PPCF  # add to sum over players in the away team
						ptot = PPCFaway[k] + PPCFhome[k]
						k += 1
					if k >= dT_array.size:
						print("Integration failed to converge: %1.3f" % (ptot))

					pitch_control[i][j] = PPCFhome[k - 1]

					end = timer()
					interval = end - start
					differential_times.append(interval)
			length_time_end = timer()
			z = length_time_end - length_time
			length_avg.append(z)

		end_total = time.time()
		elapsed_time = end_total - start_total

		print("Computing Pitch Control took {0:.2f} seconds".format(elapsed_time))
		print("AVG intercept times computation:     {0:.10f} seconds".format(np.mean(intercept_times)))
		print("AVG significant players computation: {0:.10f} seconds".format(np.mean(significant_players)))
		print("AVG differential times computation:  {0:.10f} seconds".format(np.mean(differential_times)))
		print("AVG length computation:              {0:.10f} seconds".format(np.mean(length_avg)))
		print("max: "+ str(np.max(pitch_control)))
		print("min: "+ str(np.min(pitch_control)))


		# xG = self.compute_xG()
		print('x: 2; y: 30 = ', pitch_control[30*self.pc_resolution_factor, 2*self.pc_resolution_factor])
		print('x: 50; y: 30 = ', pitch_control[30*self.pc_resolution_factor, 50*self.pc_resolution_factor])
		print('x: 95; y: 30 = ', pitch_control[30*self.pc_resolution_factor, 100*self.pc_resolution_factor])

		frame.pitch_control = pitch_control
		return pitch_control


	#Gives model of xG depending on ball possesion. min 0, max 1
	# to get value of xG in position x,y (105, 68), take pgoal_2d[y,x]
	def compute_xG(self, frame):
		xG_model = sm.load('Computations\\xG_model_result_dist_angle.pickle')

		left_half = np.zeros((53, 68))
		for x in range(53):
			for y in range(68):
				sh = dict()
				a = np.arctan(7.32 * x / (x ** 2 + abs(y - 65 / 2) ** 2 - (7.32 / 2) ** 2))
				if a < 0:
					a = np.pi + a
				sh['Angle'] = a * 180 / np.pi
				sh['Dist'] = np.sqrt(x ** 2 + abs(y - 65 / 2) ** 2)
				# sh['D2'] = x ** 2 + abs(y - 65 / 2) ** 2
				sh['X'] = x
				# sh['AX'] = x * a
				# sh['X2'] = x ** 2
				sh['Y'] = y
				# sh['C'] = abs(y - 65 / 2)
				# sh['C2'] = (y - 65 / 2) ** 2

				left_half[x, y] = self.calculate_xG(sh, xG_model.params, ['Dist', 'Angle'])

		#convert xG to match pitch image // this will give only "left" hlf of the pitch
		left_half = np.rot90(left_half)
		left_half = np.roll(left_half, -1, axis=0)
		# get right half as well
		right_half = np.rot90(left_half)
		right_half = np.rot90(right_half)

		team = self.get_team_by_team_type(frame.ball.possession)
		if team.first_half_direction == AttackDirection.RIGHT: #TODO find out which half of the frame, so that it works also for secondhalf
			left_half = np.zeros((68, 53))
		else:
			right_half = np.zeros((68, 53))

		xG = np.concatenate((left_half, right_half), axis=1)
		xG = xG[:, :-1] # remove last X column, so that xG dimensions match pitch control (68, 105)
		return xG

	def calculate_xG(self, sh, params, variables):
		bsum = params[0]
		for i, v in enumerate(variables):
			bsum = bsum + params[i + 1] * sh[v]
		xG = 1 / (1 + np.exp(bsum))
		return xG

	def compute_index(self, frame):
		pitch_control = frame.pitch_control
		if pitch_control is None:
			pitch_control = self.compute_pitch_control(frame)

		xG = self.compute_xG(frame)
		if frame.ball.possession == TeamType.AWAY: # if blue team has the ball, the pc has to be reversed (by default 1 means red has full pitch control at this point on pitch)
			pitch_control = 1 - pitch_control

		xG_scaled = np.repeat(xG, self.pc_resolution_factor, axis=1)
		xG_scaled = np.repeat(xG_scaled, self.pc_resolution_factor, axis=0)

		pitch_control = np.flip(pitch_control, axis=0)
		index = np.multiply(pitch_control, xG_scaled)

		print("My Index:")
		print("max: " + str(np.max(index)))
		print("min: " + str(np.min(index)))

		# xG = self.compute_xG()
		print('x: 2; y: 30 = ', index[30, 2 ])
		print('x: 50; y: 30 = ', index[30 , 50 ])
		print('x: 95; y: 30 = ', index[30 , 100 ])

		return index

