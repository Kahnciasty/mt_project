from objects import Ball, FrameEvent
from players import TrackingPlayer
from typedefs.AttackDirection import AttackDirection
from typedefs.Position import Position


class Frame:
	def __init__(self, id, players, ball, events):
		self.id = id
		self.players = players
		self.ball = ball
		self.frame_events = events
		self.pitch_control = None


	def __str__(self):
		return 'ball: [{}, {}, {}]'.format(self.ball.vector[0], self.ball.vector[1], self.ball.vector[2])

	def get_last_defender(self, game, teamtype):
		period_id = game.get_period_id_of_frame(self)
		team_players = [p for p in self.players if p.team_type == teamtype]

		last_defender = team_players[0]
		for track_player in team_players:
			event_player = game.get_event_player_by_tracking_player(track_player)
			if event_player.position == Position.GOALKEEPER:
				continue
			if (period_id == 1 and event_player.team.first_half_direction == AttackDirection.RIGHT) or (period_id == 2 and event_player.team.second_half_direction == AttackDirection.RIGHT):
				if track_player.coord.x < last_defender.coord.x:
					last_defender = track_player
			else:
				if track_player.coord.x > last_defender.coord.x:
					last_defender = track_player
		return last_defender


def from_line(frame_as_line) -> "Frame":
	items = frame_as_line.split(":")
	id = int(items[0])
	players = create_players(items[1])
	ball = Ball.from_line(items[2])
	events = []
	if items[3] is not "":
		events = create_events_list(items[3], id)
	return Frame(id, players, ball, events)


def create_players(param):
	items = param.split(";")
	retval = []
	for item in items[:-1]:
		retval.append(TrackingPlayer.from_line(item))
	return retval


def create_events_list(param, frame_id):
	items = param.split(";")
	retval = []
	for item in items[:-1]:
		retval.append(FrameEvent.from_line(item, frame_id))
	return retval
