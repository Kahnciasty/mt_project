from typedefs.QuallifierType import QualifierType
from my_xml.XmlAttributes import XmlAttributes


class Qualifier:
	def __init__(self, id, qualifier_id, value):
		id_int = int(qualifier_id)
		self.id = id
		self.qualifier_id = int(id_int)
		if int(id_int) == 140 or int(id_int) == 141:
			self.type = QualifierType(int(id_int))
		else:
			self.type = None
		self.value = value

	# comment this ↓ method, if you want to show event details in debug mode. Uncomment if you want to export the events to xml file
	def __dict__(self):
		ret = dict()
		ret[XmlAttributes.ID] = str(self.id)
		ret[XmlAttributes.QUALIFIER_ID] = str(self.qualifier_id)
		if self.value is not None:
			ret[XmlAttributes.VALUE] = str(self.value)
		return ret