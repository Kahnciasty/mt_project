import numpy as np


class Statistics:
	def __init__(self):
		self.pass_alignment_stats = []
		self.state_alignment_stats = []
		self.event_diff_dict = dict()
		self.mismatched_events = []
		self.corrected_performers = []
		self.pass_events = []
		self.state_changing_events = []
		self.duel_events = []
		self.recovery_events = []

	def pass_std_dev(self):
		return np.std(self.pass_alignment_stats)

	def pass_variance(self):
		return np.var(self.pass_alignment_stats)

	def pass_avg(self):
		return np.average(self.pass_alignment_stats)

	def state_std_dev(self):
		return np.std(self.state_alignment_stats)

	def state_variance(self):
		return np.var(self.state_alignment_stats)

	def state_avg(self):
		return np.average(self.state_alignment_stats)