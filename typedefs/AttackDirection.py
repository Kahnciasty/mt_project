from enum import Enum


class AttackDirection(Enum):
	LEFT = "Right to Left"
	RIGHT = "Left to Right"
