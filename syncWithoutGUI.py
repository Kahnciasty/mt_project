#!/usr/bin/python

import Constants
import Launcher
from my_xml.XmlWriter import XmlWriter
from sequence.Aligner import Aligner
import sys


def synchronize(dir_path, game_id):
	print('Initializing game {id}'.format(id=game_id))
	#Load the game
	game = Launcher.initiate(dir_path, game_id)
	game.load()
	print('Game {id} loaded'.format(id=game_id))

	print('Begin synchronization')
	# Synchronize
	generator = Aligner(game)
	game.load_xml_data(use_original_file=True)
	generator.generate_file_without_synchronization()
	generator.adjust_events()

	#Save results
	xmlWriter = XmlWriter(dir_path, game)
	root = xmlWriter.generate_event_details_xml(game)
	text = xmlWriter.prettify(root)
	orig = "<?my_xml version=\"1.0\" ?>"
	text2 = text.replace(orig, Constants.xml_defining_string_16)
	path = game.event_data_improved_path
	with open(path, 'w') as file:
		file.write(text2)

	print('Files with synchronized data have been created in {path}'.format(path=dir_path))


# example command : python syncWithoutGUI.py D:\path\to\directory\with\files 1059337
if __name__ == '__main__':
	print('Argument List:', str(sys.argv))

	path = sys.argv[1]
	game_id = sys.argv[2]

	synchronize(path, game_id)